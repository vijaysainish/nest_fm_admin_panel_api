export interface UserComments {
    user: string;
    comment: string;
    date: Date;
}
