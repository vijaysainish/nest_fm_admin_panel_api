import { UserRole } from './../enums/user.enums';
export interface JwtPayload {
    id: any;
    role: UserRole;
    email: string;
    isActive: boolean;
}
