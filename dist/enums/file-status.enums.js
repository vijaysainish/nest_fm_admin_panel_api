"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileStatus = void 0;
var FileStatus;
(function (FileStatus) {
    FileStatus["Pending"] = "Pending";
    FileStatus["Deleted"] = "Deleted";
    FileStatus["Completed"] = "Completed";
    FileStatus["Inactive"] = "Inactive";
    FileStatus["InProgress"] = "InProgress";
    FileStatus["Approved"] = "Approved";
})(FileStatus = exports.FileStatus || (exports.FileStatus = {}));
//# sourceMappingURL=file-status.enums.js.map