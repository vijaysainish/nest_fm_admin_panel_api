"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignatureStatus = void 0;
var SignatureStatus;
(function (SignatureStatus) {
    SignatureStatus["Pending"] = "Pending";
    SignatureStatus["Completed"] = "Completed";
    SignatureStatus["Declined"] = "Declined";
})(SignatureStatus = exports.SignatureStatus || (exports.SignatureStatus = {}));
//# sourceMappingURL=signature-status.enums.js.map