import { Strategy } from 'passport-jwt';
import { UnauthorizedException } from '@nestjs/common';
import { JwtPayload } from 'src/interfaces/jwt-payload.interface';
import { UserRepository } from 'src/modules/users/repository/user.repository';
declare const JwtStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtStrategy extends JwtStrategy_base {
    private userRepos;
    constructor(userRepos: UserRepository);
    validate(payload: JwtPayload): Promise<import("../modules/users/entities/user.entity").User | UnauthorizedException>;
}
export {};
