"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommonService = void 0;
const common_1 = require("@nestjs/common");
const path_1 = require("path");
let CommonService = class CommonService {
    randomNumber(length) {
        const string = '0123456789';
        return this.iterateString(string, length);
    }
    randomString(length) {
        const string = '0123456789qwertyu+_)(*&^%$#@!}{[]|:;><.,/?ioplkjhgfdsazxcvbnmMNBVCXZASDFGHJKLPOIUYTREWQ';
        return this.iterateString(string, length);
    }
    randomString2(length) {
        const string = '123456789qwertyuioplkjhgfdsazxcvbnmMNBVCXZASDFGHJKLPOIUYTREWQ';
        return this.iterateString(string, length);
    }
    iterateString(string, length) {
        const stringLength = string.length;
        let generatedValue = '';
        while (generatedValue.length < length) {
            generatedValue += string.charAt(Math.floor(Math.random() * stringLength));
        }
        return generatedValue;
    }
    getNewName(file) {
        let name = file.split('.')[0];
        const fileExtName = (0, path_1.extname)(file);
        name = name.split(' ').join('-');
        const randomName = Array(10)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
        return [
            `${name}-${randomName}${fileExtName}`,
            `${fileExtName.slice(1, fileExtName.length)}`,
        ];
    }
};
CommonService = __decorate([
    (0, common_1.Injectable)()
], CommonService);
exports.CommonService = CommonService;
//# sourceMappingURL=common.service.js.map