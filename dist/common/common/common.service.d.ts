export declare class CommonService {
    randomNumber(length: number): string;
    randomString(length: number): string;
    randomString2(length: number): string;
    iterateString(string: any, length: any): string;
    getNewName(file: any): string[];
}
