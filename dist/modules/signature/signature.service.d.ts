import { FileManagementService } from './../file-management/file-management.service';
import { UsersService } from './../users/user.service';
import { SignatureRepository } from './repository/signature.repository';
import { CreateSignatureDto } from './dto/create-signature.dto';
import { UpdateSignatureDto } from './dto/update-signature.dto';
export declare class SignatureService {
    private signatureRepository;
    private usersService;
    private fileService;
    getFileSignatures(id: string): Promise<import("./entities/signature.entity").Signature[]>;
    constructor(signatureRepository: SignatureRepository, usersService: UsersService, fileService: FileManagementService);
    create(createSignatureDto: CreateSignatureDto): Promise<import("./entities/signature.entity").Signature>;
    findAll(): string;
    findOne(id: number): string;
    update(id: number, updateSignatureDto: UpdateSignatureDto): string;
    remove(id: number): string;
}
