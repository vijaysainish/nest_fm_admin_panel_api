import { BaseEntity } from 'typeorm';
import { SignatureStatus } from './../../../enums/signature-status.enums';
export declare class Signature extends BaseEntity {
    id: any;
    file: string;
    userToken: string;
    index: number;
    user: string;
    userName: string;
    userEmail: string;
    status: SignatureStatus;
    comment: string;
    signFile: string;
}
