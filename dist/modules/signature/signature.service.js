"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignatureService = void 0;
const file_management_service_1 = require("./../file-management/file-management.service");
const user_service_1 = require("./../users/user.service");
const signature_repository_1 = require("./repository/signature.repository");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
let SignatureService = class SignatureService {
    constructor(signatureRepository, usersService, fileService) {
        this.signatureRepository = signatureRepository;
        this.usersService = usersService;
        this.fileService = fileService;
    }
    async getFileSignatures(id) {
        console.log(id);
        return await this.signatureRepository.find({ where: { file: id } });
    }
    async create(createSignatureDto) {
        const { userId, fileId, userEmail } = createSignatureDto;
        let user;
        if (userId) {
            user = await this.usersService.getUserForSign(userId);
        }
        const file = await this.fileService.getfile(fileId);
        if (!user && !userEmail)
            throw new common_1.BadRequestException('Please enter user email');
        return this.signatureRepository.newSigns(createSignatureDto, user, file);
    }
    findAll() {
        return `This action returns all signature`;
    }
    findOne(id) {
        return `This action returns a #${id} signature`;
    }
    update(id, updateSignatureDto) {
        return `This action updates a #${id} signature`;
    }
    remove(id) {
        return `This action removes a #${id} signature`;
    }
};
SignatureService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(signature_repository_1.SignatureRepository)),
    __metadata("design:paramtypes", [signature_repository_1.SignatureRepository,
        user_service_1.UsersService,
        file_management_service_1.FileManagementService])
], SignatureService);
exports.SignatureService = SignatureService;
//# sourceMappingURL=signature.service.js.map