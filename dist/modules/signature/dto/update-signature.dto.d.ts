import { CreateSignatureDto } from './create-signature.dto';
declare const UpdateSignatureDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateSignatureDto>>;
export declare class UpdateSignatureDto extends UpdateSignatureDto_base {
}
export {};
