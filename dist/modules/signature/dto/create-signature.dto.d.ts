export declare class CreateSignatureDto {
    userId: string;
    fileId: string;
    userName: string;
    userEmail: string;
    signatureFileName: string;
}
