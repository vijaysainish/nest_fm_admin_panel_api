import { ShareFiles } from 'src/modules/file-management/entities/file-management.entity';
import { User } from 'src/modules/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateSignatureDto } from './../dto/create-signature.dto';
import { Signature } from './../entities/signature.entity';
export declare class SignatureRepository extends Repository<Signature> {
    newSigns(createSignature: CreateSignatureDto, user: User, file: ShareFiles): Promise<Signature>;
}
