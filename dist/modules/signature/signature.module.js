"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignatureModule = void 0;
const file_management_service_1 = require("./../file-management/file-management.service");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_repository_1 = require("../users/repository/user.repository");
const file_management_repository_1 = require("../file-management/repository/file-management.repository");
const file_management_module_1 = require("./../file-management/file-management.module");
const user_service_1 = require("./../users/user.service");
const signature_repository_1 = require("./repository/signature.repository");
const signature_controller_1 = require("./signature.controller");
const signature_service_1 = require("./signature.service");
const user_shared_files_repositroy_1 = require("../file-management/repository/user-shared-files.repositroy");
const file_comment_repositroy_1 = require("../file-management/repository/file-comment.repositroy");
const notifications_repository_1 = require("../notifications/repository/notifications.repository");
let SignatureModule = class SignatureModule {
};
SignatureModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                signature_repository_1.SignatureRepository,
                user_repository_1.UserRepository,
                file_management_repository_1.FileManagementRepository,
                user_shared_files_repositroy_1.SharedUserRepository,
                file_comment_repositroy_1.CommentRepository,
                notifications_repository_1.NotificationRepository
            ]),
            file_management_module_1.FileManagementModule,
        ],
        controllers: [signature_controller_1.SignatureController],
        providers: [signature_service_1.SignatureService, user_service_1.UsersService, file_management_service_1.FileManagementService],
    })
], SignatureModule);
exports.SignatureModule = SignatureModule;
//# sourceMappingURL=signature.module.js.map