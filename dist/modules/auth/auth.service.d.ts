import { MailerService } from '@nestjs-modules/mailer';
import { JwtService } from '@nestjs/jwt';
import { User } from '../users/entities/user.entity';
import { UserRepository } from '../users/repository/user.repository';
import { CommonService } from './../../common/common/common.service';
import { CreateUserDto } from './../users/dto/create-user.dto';
import { ChangePassDto } from './dto/change-pass.dto';
import { ForgotPassDto } from './dto/forgot-pass.dto';
import { LoginDto } from './dto/login.dto';
import { ResetPassDto } from './dto/reset-pass.dto';
export declare class AuthService {
    private cs;
    private jwtService;
    private readonly mailerService;
    private userRepos;
    constructor(cs: CommonService, jwtService: JwtService, mailerService: MailerService, userRepos: UserRepository);
    login(loginDto: LoginDto): Promise<{
        user: User;
        accessToken: string;
    }>;
    create(createAuthDto: CreateUserDto): Promise<{
        user: User;
        accessToken: string;
    }>;
    signToken(response: User): Promise<{
        user: User;
        accessToken: string;
    }>;
    forgotPass(forgotPassDto: ForgotPassDto): Promise<{
        message: string;
    }>;
    resetPass(resetPassDto: ResetPassDto): Promise<{
        message: string;
    }>;
    changePass(changePassDto: ChangePassDto, user: User): Promise<{
        message: string;
    }>;
}
