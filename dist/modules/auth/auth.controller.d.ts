import { ResetPassDto } from './dto/reset-pass.dto';
import { CreateUserDto } from './../users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { ForgotPassDto } from './dto/forgot-pass.dto';
import { LoginDto } from './dto/login.dto';
import { ChangePassDto } from './dto/change-pass.dto';
import { User } from '../users/entities/user.entity';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    create(adminSignUpDto: CreateUserDto): Promise<{
        user: User;
        accessToken: string;
    }>;
    login(loginDto: LoginDto): Promise<{
        user: User;
        accessToken: string;
    }>;
    forgotPass(forgotPassDto: ForgotPassDto): Promise<{
        message: string;
    }>;
    resetPass(resetPassDto: ResetPassDto): Promise<{
        message: string;
    }>;
    changePass(changePassDto: ChangePassDto, user: User): Promise<{
        message: string;
    }>;
}
