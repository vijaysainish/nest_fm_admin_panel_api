"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const auth_guard_1 = require("./auth.guard");
const reset_pass_dto_1 = require("./dto/reset-pass.dto");
const common_1 = require("@nestjs/common");
const create_user_dto_1 = require("./../users/dto/create-user.dto");
const auth_service_1 = require("./auth.service");
const forgot_pass_dto_1 = require("./dto/forgot-pass.dto");
const login_dto_1 = require("./dto/login.dto");
const change_pass_dto_1 = require("./dto/change-pass.dto");
const getUser_decorator_1 = require("../../decorators/getUser.decorator");
const user_entity_1 = require("../users/entities/user.entity");
let AuthController = class AuthController {
    constructor(authService) {
        this.authService = authService;
    }
    create(adminSignUpDto) {
        return this.authService.create(adminSignUpDto);
    }
    login(loginDto) {
        return this.authService.login(loginDto);
    }
    forgotPass(forgotPassDto) {
        return this.authService.forgotPass(forgotPassDto);
    }
    resetPass(resetPassDto) {
        return this.authService.resetPass(resetPassDto);
    }
    changePass(changePassDto, user) {
        return this.authService.changePass(changePassDto, user);
    }
};
__decorate([
    (0, common_1.Post)('/admin/register'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_user_dto_1.CreateUserDto]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "create", null);
__decorate([
    (0, common_1.Post)(''),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [login_dto_1.LoginDto]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "login", null);
__decorate([
    (0, common_1.Post)('forgot-password'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [forgot_pass_dto_1.ForgotPassDto]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "forgotPass", null);
__decorate([
    (0, common_1.Patch)('reset-password'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [reset_pass_dto_1.ResetPassDto]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "resetPass", null);
__decorate([
    (0, common_1.Post)('change-password'),
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [change_pass_dto_1.ChangePassDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "changePass", null);
AuthController = __decorate([
    (0, common_1.Controller)('auth'),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map