import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UsersService } from '../users/user.service';
export declare class RoleGuard implements CanActivate {
    private readonly reflector;
    private readonly usersService;
    constructor(reflector: Reflector, usersService: UsersService);
    canActivate(context: ExecutionContext): Promise<boolean>;
    validateRole(user: any, context: any): Promise<boolean>;
}
