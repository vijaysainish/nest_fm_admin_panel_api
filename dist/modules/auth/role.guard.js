"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleGuard = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const jwt = require("jsonwebtoken");
const user_service_1 = require("../users/user.service");
let RoleGuard = class RoleGuard {
    constructor(reflector, usersService) {
        this.reflector = reflector;
        this.usersService = usersService;
    }
    async canActivate(context) {
        const request = context.switchToHttp().getRequest();
        if (request) {
            if (!request.headers.authorization) {
                return false;
            }
            console.log(request.headers.authorization);
            const auth = request.headers.authorization;
            const token = auth.split(' ')[1];
            const decoded = await jwt.verify(token, '#E$8>%eAXpEQsPh[5spbXUhCkNs;C$]KSAjfM0Q>gfV<inK(1zjR|16:)4g.M@@1SlI{LSF8RKWXM%/OWn&|9;<,L[GO,PPyyAWdG<J)c6a&2mHZdu62rqq+gQ|DkF_Z/*tTbB!aIH1c{o,$.wniFz');
            let is_acitve = await this.validateRole(decoded, context);
            if (is_acitve) {
                return true;
            }
            else {
                throw new common_1.HttpException('Unauthorized request', common_1.HttpStatus.UNAUTHORIZED);
            }
        }
        else {
            return true;
        }
    }
    async validateRole(user, context) {
        let res = await this.usersService.findActive(user.id);
        console.log('res', res);
        return res;
    }
};
RoleGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [core_1.Reflector, user_service_1.UsersService])
], RoleGuard);
exports.RoleGuard = RoleGuard;
//# sourceMappingURL=role.guard.js.map