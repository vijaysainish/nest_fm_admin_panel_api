"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const mailer_1 = require("@nestjs-modules/mailer");
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("../users/entities/user.entity");
const user_repository_1 = require("../users/repository/user.repository");
const common_service_1 = require("./../../common/common/common.service");
let AuthService = class AuthService {
    constructor(cs, jwtService, mailerService, userRepos) {
        this.cs = cs;
        this.jwtService = jwtService;
        this.mailerService = mailerService;
        this.userRepos = userRepos;
    }
    async login(loginDto) {
        const response = await this.userRepos.validateUser(loginDto);
        if (!response.isActive) {
            throw new common_1.HttpException('Your account is deactivated, contact to admin.', common_1.HttpStatus.UNAUTHORIZED);
        }
        return this.signToken(response);
    }
    async create(createAuthDto) {
        const result = await this.userRepos.userRegister(createAuthDto, true);
        await this.mailerService
            .sendMail({
            to: result.email,
            subject: 'File Share Registration',
            template: 'src/templates/registration.pug',
            context: {
                name: result.firstName + ' ' + (result.lastName || ''),
                url: ``,
            },
        })
            .then(() => console.log('success'))
            .catch((e) => console.log(e));
        return this.signToken(result);
    }
    async signToken(response) {
        const { email, id, role, isActive } = response;
        delete response.resetPasswordToken;
        delete response.password;
        delete response.salt;
        delete response.deletedAt;
        const payload = {
            id,
            email,
            role,
            isActive
        };
        const accessToken = await this.jwtService.sign(payload);
        return {
            user: response,
            accessToken,
        };
    }
    async forgotPass(forgotPassDto) {
        const result = await this.userRepos.findOne({
            email: forgotPassDto.email,
            isActive: true,
        });
        if (!result) {
            throw new common_1.NotFoundException('No User Found');
        }
        result.resetPasswordToken = this.cs.randomString2(20);
        console.log(result.resetPasswordToken);
        await result.save();
        await this.mailerService
            .sendMail({
            to: result.email,
            subject: 'Reset Password',
            template: 'src/templates/forgot-password.pug',
            context: {
                name: result.firstName + ' ' + (result.lastName || ''),
                url: `https://dms.miraclechd.co/auth/reset-password/${result.resetPasswordToken}`,
            },
        })
            .then(() => console.log('success'))
            .catch((e) => console.log(e));
        return {
            message: 'Forgot Password requested Successfully',
        };
    }
    async resetPass(resetPassDto) {
        const result = await this.userRepos.findOne({
            resetPasswordToken: resetPassDto.resetToken,
            isActive: true,
        });
        if (!result) {
            throw new common_1.NotFoundException('No User Found');
        }
        const passwordHash = (await this.userRepos.genHash(result.salt, resetPassDto.password)).toString();
        result.password = passwordHash;
        result.resetPasswordToken = null;
        await result.save();
        return {
            message: 'Password changed successfully',
        };
    }
    async changePass(changePassDto, user) {
        const isValidUser = await this.userRepos.validateUser({
            email: user.email,
            password: changePassDto.oldPassword,
        });
        if (isValidUser instanceof user_entity_1.User) {
            const passwordHash = (await this.userRepos.genHash(user.salt, changePassDto.newPassword)).toString();
            user.password = passwordHash;
            await user.save();
            return {
                message: 'Password changed Successfully',
            };
        }
        throw new common_1.InternalServerErrorException('Something went wrong');
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __param(3, (0, typeorm_1.InjectRepository)(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [common_service_1.CommonService,
        jwt_1.JwtService,
        mailer_1.MailerService,
        user_repository_1.UserRepository])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map