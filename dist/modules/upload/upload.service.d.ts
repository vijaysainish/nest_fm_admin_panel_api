import { StreamableFile } from '@nestjs/common';
import { CommonService } from './../../common/common/common.service';
export declare class UploadService {
    private cs;
    constructor(cs: CommonService);
    uploadImage(img: any): Promise<any>;
    getFile(fileName: any, folder: any): Promise<StreamableFile>;
    uploadPDF(pdf: any): Promise<any>;
}
