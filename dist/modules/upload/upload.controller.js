"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const user_entity_1 = require("./../users/entities/user.entity");
const multer_1 = require("multer");
const getUser_decorator_1 = require("../../decorators/getUser.decorator");
const upload_service_1 = require("./upload.service");
let UploadController = class UploadController {
    constructor(uploadService) {
        this.uploadService = uploadService;
    }
    image(file, user) {
        console.log(file);
        return file;
    }
    findImg(img) {
        return this.uploadService.getFile(img, 'images');
    }
    findPpdf(pdf) {
        return this.uploadService.getFile(pdf, 'pdfs');
    }
    pdf(pdf) {
        return this.uploadService.uploadPDF(pdf);
    }
};
__decorate([
    (0, common_1.Post)('upload/image'),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file', {
        storage: (0, multer_1.diskStorage)({
            destination: './uploads/images',
        }),
    })),
    __param(0, (0, common_1.UploadedFile)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], UploadController.prototype, "image", null);
__decorate([
    (0, common_1.Get)('/image/:img'),
    __param(0, (0, common_1.Param)('img')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], UploadController.prototype, "findImg", null);
__decorate([
    (0, common_1.Get)('/pdf/:pdf'),
    __param(0, (0, common_1.Param)('pdf')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], UploadController.prototype, "findPpdf", null);
__decorate([
    (0, common_1.Post)('upload/pdf'),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('pdf')),
    __param(0, (0, common_1.UploadedFile)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], UploadController.prototype, "pdf", null);
UploadController = __decorate([
    (0, common_1.Controller)('file'),
    __metadata("design:paramtypes", [upload_service_1.UploadService])
], UploadController);
exports.UploadController = UploadController;
//# sourceMappingURL=upload.controller.js.map