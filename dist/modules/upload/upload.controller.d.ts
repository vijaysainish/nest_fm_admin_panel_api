/// <reference types="multer" />
import { User } from './../users/entities/user.entity';
import { UploadService } from './upload.service';
export declare class UploadController {
    private readonly uploadService;
    constructor(uploadService: UploadService);
    image(file: Express.Multer.File, user: User): Express.Multer.File;
    findImg(img: string): Promise<import("@nestjs/common").StreamableFile>;
    findPpdf(pdf: string): Promise<import("@nestjs/common").StreamableFile>;
    pdf(pdf: any): Promise<any>;
}
