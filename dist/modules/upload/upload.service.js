"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadService = void 0;
const common_1 = require("@nestjs/common");
const AWS = require("aws-sdk");
const common_service_1 = require("./../../common/common/common.service");
const s3 = new AWS.S3({
    accessKeyId: 'AKIAYHUDVK2HX7TDAIQ3',
    secretAccessKey: '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',
});
let UploadService = class UploadService {
    constructor(cs) {
        this.cs = cs;
    }
    async uploadImage(img) {
        const [name, extension] = this.cs.getNewName(img.originalname);
        const params = {
            Bucket: 'force-dev/images',
            Key: name,
            Body: img.buffer,
        };
        return await s3
            .upload(params)
            .promise()
            .then((data) => data, (err) => err);
    }
    async getFile(fileName, folder) {
        const params = {
            Bucket: `force-dev/${folder}`,
            Key: fileName,
        };
        return await s3
            .headObject(params)
            .promise()
            .then(() => {
            let stream = s3.getObject(params).createReadStream();
            return new common_1.StreamableFile(stream);
        })
            .catch((error) => {
            throw new Error(error);
        });
    }
    async uploadPDF(pdf) {
        const [name, extension] = this.cs.getNewName(pdf.originalname);
        const params = {
            Bucket: 'force-dev/pdfs',
            Key: name,
            Body: pdf.buffer,
        };
        return await s3
            .upload(params)
            .promise()
            .then((data) => data, (err) => err);
    }
};
UploadService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [common_service_1.CommonService])
], UploadService);
exports.UploadService = UploadService;
//# sourceMappingURL=upload.service.js.map