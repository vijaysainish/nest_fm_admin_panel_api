"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileManagementModule = void 0;
const user_repository_1 = require("../users/repository/user.repository");
const user_service_1 = require("./../users/user.service");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const signature_repository_1 = require("./../signature/repository/signature.repository");
const signature_service_1 = require("./../signature/signature.service");
const file_management_controller_1 = require("./file-management.controller");
const file_management_service_1 = require("./file-management.service");
const file_management_repository_1 = require("./repository/file-management.repository");
const user_shared_files_repositroy_1 = require("./repository/user-shared-files.repositroy");
const file_comment_repositroy_1 = require("./repository/file-comment.repositroy");
const notifications_repository_1 = require("../notifications/repository/notifications.repository");
let FileManagementModule = class FileManagementModule {
};
FileManagementModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                file_management_repository_1.FileManagementRepository,
                signature_repository_1.SignatureRepository,
                user_repository_1.UserRepository,
                user_shared_files_repositroy_1.SharedUserRepository,
                file_comment_repositroy_1.CommentRepository,
                notifications_repository_1.NotificationRepository
            ]),
        ],
        controllers: [file_management_controller_1.FileManagementController],
        providers: [file_management_service_1.FileManagementService, signature_service_1.SignatureService, user_service_1.UsersService],
    })
], FileManagementModule);
exports.FileManagementModule = FileManagementModule;
//# sourceMappingURL=file-management.module.js.map