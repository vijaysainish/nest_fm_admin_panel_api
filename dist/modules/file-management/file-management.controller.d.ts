/// <reference types="multer" />
import { SignatureService } from '../signature/signature.service';
import { User } from '../users/entities/user.entity';
import { CreateFileManagementDto } from './dto/create-file-management.dto';
import { ShareFileDto } from './dto/share-file.dto';
import { UpdateFileManagementDto } from './dto/update-file-management.dto';
import { FileManagementService } from './file-management.service';
import { UpdateFileRequestDto } from './dto/update-file-request.dto copy';
import { SearchFileRequestDto } from './dto/search.dto';
import { UpdateOtherUserFiletDto } from './dto/update-other-user-file.dto';
import { saveEditFileDto } from './dto/save-edit-file.dto';
export declare class FileManagementController {
    private readonly fileManagementService;
    private readonly signService;
    constructor(fileManagementService: FileManagementService, signService: SignatureService);
    create(createFileManagementDto: CreateFileManagementDto, user: User): Promise<import("./entities/file-management.entity").ShareFiles>;
    getFiles(user: User, page: any, limit: any): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    }>;
    getSharingFiles(user: User, page: any, limit: any): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    }>;
    getSharedWithMeFiles(user: User, page: any, limit: any): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    } | {
        list: import("./entities/user-shared-files.entity").SharedUser[];
        total: number;
    }>;
    getSharedFile(id: any, user: User): Promise<{
        shared_users: any;
        comments: any;
        shared_file: any;
    }>;
    getSharedWithMeFile(id: any, user: User): Promise<{
        shared_file: any;
    }>;
    getOhterUserFile(name: any, email: any, file_id: any): Promise<import("./entities/user-shared-files.entity").SharedUser>;
    getInitiatorFileDetails(id: any, user: User): Promise<{
        shared_file: any;
    }>;
    findOne(id: string): Promise<{
        signatures: import("../signature/entities/signature.entity").Signature[];
        id: number;
        initiator_id: number;
        fileName: string;
        originalFileName: string;
        startDate: Date;
        endDate: Date;
        status: import("../../enums/file-status.enums").FileStatus;
        is_sharing: number;
        is_editable: number;
        createdAt: Date;
        updatedAt: Date;
        deletedAt: Date;
        initiator: User[];
        comment: import("./entities/file-comment.entity").FileComment[];
        shared_users: import("./entities/user-shared-files.entity").SharedUser[];
    }>;
    sharefile(id: string, shareFileDto: ShareFileDto, user: User): Promise<import("./entities/file-management.entity").ShareFiles>;
    acceptRejectFileRequest(UpdateRequestDto: UpdateFileRequestDto, user: User): Promise<import("./entities/file-management.entity").ShareFiles>;
    updateFileStatus(updateFileManagementDto: UpdateFileManagementDto, user: User): Promise<import("typeorm").UpdateResult>;
    uploadFile(file: Express.Multer.File): Express.Multer.File;
    addFile(filedata: CreateFileManagementDto, user: User): Promise<import("./entities/file-management.entity").ShareFiles>;
    saveFile(filedata: CreateFileManagementDto, user: User): Promise<import("./entities/file-management.entity").ShareFiles>;
    search(searchdata: SearchFileRequestDto, user: User): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    }>;
    searchSharedWithMe(searchdata: SearchFileRequestDto, user: User): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    }>;
    searchSharingFile(searchdata: SearchFileRequestDto, user: User): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    }>;
    updateFile(filedata: UpdateFileManagementDto, user: User): Promise<{
        message: string;
    }>;
    updateOtherUserFile(filedata: UpdateOtherUserFiletDto): Promise<{
        message: string;
    }>;
    editReshareFile(filedata: UpdateFileManagementDto, user: User): Promise<{
        message: string;
    }>;
    editSharingFile(filedata: saveEditFileDto, user: User): Promise<{
        message: string;
    }>;
    shareFileFromList(filedata: ShareFileDto, user: User): Promise<import("./entities/user-shared-files.entity").SharedUser[]>;
    deleteFile(data: any, user: User): Promise<void>;
    seeUploadedFile(image: any, res: any): any;
}
