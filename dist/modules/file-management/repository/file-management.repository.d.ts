import { Repository } from 'typeorm';
import { CreateFileManagementDto } from '../dto/create-file-management.dto';
import { UpdateFileManagementDto } from '../dto/update-file-management.dto';
import { ShareFiles } from './../entities/file-management.entity';
export declare class FileManagementRepository extends Repository<ShareFiles> {
    newFile(createFileManagementDto: CreateFileManagementDto, userId: number, is_sharing?: number): Promise<ShareFiles>;
    updateFile(updateFileManagementDto: UpdateFileManagementDto, userId: number): Promise<ShareFiles>;
}
