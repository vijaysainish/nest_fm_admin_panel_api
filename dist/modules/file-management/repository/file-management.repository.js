"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileManagementRepository = void 0;
const common_1 = require("@nestjs/common");
const file_status_enums_1 = require("../../../enums/file-status.enums");
const typeorm_1 = require("typeorm");
const file_management_entity_1 = require("./../entities/file-management.entity");
let FileManagementRepository = class FileManagementRepository extends typeorm_1.Repository {
    async newFile(createFileManagementDto, userId, is_sharing = 0) {
        const file_data = createFileManagementDto.FileData;
        const newFile = new file_management_entity_1.ShareFiles();
        newFile.fileName = file_data.filename;
        newFile.originalFileName = file_data.originalFileName;
        newFile.startDate = file_data.startDate;
        newFile.endDate = file_data.endDate;
        newFile.is_sharing = is_sharing;
        newFile.is_editable = is_sharing ? 1 : 0;
        newFile.status = is_sharing ? file_status_enums_1.FileStatus.Pending : file_status_enums_1.FileStatus.InProgress;
        newFile.initiator_id = userId;
        try {
            await newFile.save();
            return newFile;
        }
        catch (e) {
            console.log(e);
            throw new common_1.InternalServerErrorException('Something Went wrong');
        }
    }
    async updateFile(updateFileManagementDto, userId) {
        const file_id = updateFileManagementDto.file_id;
        const filename = updateFileManagementDto.filename;
        const newFile = new file_management_entity_1.ShareFiles();
        try {
            await newFile.save();
            return newFile;
        }
        catch (e) {
            console.log(e);
            throw new common_1.InternalServerErrorException('Something Went wrong');
        }
    }
};
FileManagementRepository = __decorate([
    (0, typeorm_1.EntityRepository)(file_management_entity_1.ShareFiles)
], FileManagementRepository);
exports.FileManagementRepository = FileManagementRepository;
//# sourceMappingURL=file-management.repository.js.map