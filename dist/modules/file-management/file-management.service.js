"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileManagementService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const file_management_repository_1 = require("./repository/file-management.repository");
const user_shared_files_repositroy_1 = require("./repository/user-shared-files.repositroy");
const file_comment_repositroy_1 = require("./repository/file-comment.repositroy");
const notifications_repository_1 = require("../notifications/repository/notifications.repository");
const file_status_enums_1 = require("../../enums/file-status.enums");
const typeorm_2 = require("typeorm");
const mailer_1 = require("@nestjs-modules/mailer");
const user_enums_1 = require("../../enums/user.enums");
let FileManagementService = class FileManagementService {
    constructor(fileRepo, sharedUserRepo, commentRepo, notifyRepo, mailerService) {
        this.fileRepo = fileRepo;
        this.sharedUserRepo = sharedUserRepo;
        this.commentRepo = commentRepo;
        this.notifyRepo = notifyRepo;
        this.mailerService = mailerService;
    }
    async create(createFileManagementDto, user) {
        const new_file = await this.fileRepo.newFile(createFileManagementDto, user.id);
        const shared_user = createFileManagementDto.SharedUser;
        const file_data = createFileManagementDto.FileData;
        let shared_data_arr = [];
        let notify_data_arr = [];
        let comment_data_arr = [{
                comment: file_data.comment,
                file_id: new_file.id,
                user_id: user.id
            }];
        let send_to;
        await shared_user.user_ids.forEach(async (id, i) => {
            const data = {
                file_id: new_file.id,
                user_id: typeof id == 'number' ? id : null,
                other_user_name: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[1],
                other_user_email: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[0],
                initiator_id: user.id,
                is_shared: 1,
                is_locked: i == 0 ? 0 : 1
            };
            const notify_data = {
                send_to: typeof id == 'number' ? id : null,
                send_by: user.id,
                status: 1,
                is_unread: 1,
                request_sent: 1,
                file_id: new_file.id
            };
            shared_data_arr.push(data);
            notify_data_arr.push(notify_data);
            if (typeof id != 'number') {
                await this.mailerService
                    .sendMail({
                    to: id.split(/(?<=^\S+)\s/)[0],
                    subject: 'File Shared for Sign',
                    template: `src/templates/other-signer-user.pug`,
                    context: {
                        name: id.split(/(?<=^\S+)\s/)[1],
                        email: id.split(/(?<=^\S+)\s/)[0],
                        start_date: createFileManagementDto.FileData.startDate,
                        end_date: createFileManagementDto.FileData.endDate,
                        url: `https://dms.miraclechd.co/other-user?id=${new_file.id}&name=${id.split(/(?<=^\S+)\s/)[1]}&email=${id.split(/(?<=^\S+)\s/)[0]}`
                    },
                })
                    .then(() => console.log('success'))
                    .catch((e) => console.log(e));
            }
        });
        await this.sharedUserRepo.save(shared_data_arr);
        if (file_data.comment) {
            await this.commentRepo.save(comment_data_arr);
        }
        await this.notifyRepo.save(notify_data_arr);
        return new_file;
    }
    async saveFile(createFileManagementDto, user) {
        const new_file = await this.fileRepo.newFile(createFileManagementDto, user.id, 1);
        const shared_user = createFileManagementDto.SharedUser;
        const file_data = createFileManagementDto.FileData;
        let shared_data_arr = [];
        let comment_data_arr = [{
                comment: file_data.comment,
                file_id: new_file.id,
                user_id: user.id
            }];
        let send_to;
        await shared_user.user_ids.forEach(async (id, i) => {
            const data = {
                file_id: new_file.id,
                user_id: typeof id == 'number' ? id : null,
                other_user_name: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[1],
                other_user_email: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[0],
                initiator_id: user.id,
                is_shared: 1,
                is_locked: i == 0 ? 0 : 1
            };
            shared_data_arr.push(data);
        });
        await this.sharedUserRepo.save(shared_data_arr);
        if (file_data.comment) {
            await this.commentRepo.save(comment_data_arr);
        }
        return new_file;
    }
    async updateFile(updateFileManagementDto, user) {
        const result = await this.fileRepo.findOne({
            id: updateFileManagementDto.file_id
        });
        const shared_user = await this.sharedUserRepo.findOne({
            file_id: updateFileManagementDto.file_id,
            user_id: user.id
        });
        if (result && shared_user) {
            result.fileName = updateFileManagementDto.filename;
            await result.save();
            shared_user.is_locked = 1;
            shared_user.sign_status = file_status_enums_1.FileStatus.Completed;
            await shared_user.save();
            const next_user = await this.sharedUserRepo.findOne({
                file_id: updateFileManagementDto.file_id,
                sign_status: file_status_enums_1.FileStatus.Pending
            });
            if (next_user) {
                next_user.is_locked = 0;
                await next_user.save();
                let notify_data_arr = [{
                        send_to: next_user.initiator_id,
                        send_by: user.id,
                        status: 2,
                        is_unread: 1,
                        file_id: updateFileManagementDto.file_id
                    },
                    {
                        send_by: next_user.initiator_id,
                        send_to: next_user.user_id,
                        status: 3,
                        is_unread: 1,
                        file_id: updateFileManagementDto.file_id
                    }
                ];
                await this.notifyRepo.save(notify_data_arr);
            }
            else {
                let notify_data_arr = [{
                        send_to: shared_user.initiator_id,
                        send_by: user.id,
                        status: 2,
                        is_unread: 1,
                        file_id: updateFileManagementDto.file_id
                    }];
                result.status = file_status_enums_1.FileStatus.Completed;
                await result.save();
                await this.notifyRepo.save(notify_data_arr);
            }
        }
        else {
            throw new common_1.NotFoundException('No User Found');
        }
        return {
            message: 'File has been updated successfully',
        };
    }
    async updateOtherUserFile(updateOtherUserFiletDto) {
        const result = await this.fileRepo.findOne({
            id: updateOtherUserFiletDto.file_id
        });
        const shared_user = await this.sharedUserRepo.findOne({
            file_id: updateOtherUserFiletDto.file_id,
            other_user_email: updateOtherUserFiletDto.email,
            other_user_name: updateOtherUserFiletDto.name
        });
        if (result && shared_user) {
            result.fileName = updateOtherUserFiletDto.filename;
            await result.save();
            shared_user.is_locked = 1;
            shared_user.sign_status = file_status_enums_1.FileStatus.Completed;
            await shared_user.save();
            const next_user = await this.sharedUserRepo.findOne({
                file_id: updateOtherUserFiletDto.file_id,
                sign_status: file_status_enums_1.FileStatus.Pending
            });
            if (next_user) {
                next_user.is_locked = 0;
                await next_user.save();
                let notify_data_arr = [{
                        send_to: next_user.initiator_id,
                        send_by: null,
                        status: 2,
                        is_unread: 1,
                        file_id: updateOtherUserFiletDto.file_id,
                        message: `${updateOtherUserFiletDto.name}, Other user updated signed on file`
                    },
                    {
                        send_by: next_user.initiator_id,
                        send_to: next_user.user_id,
                        status: 3,
                        is_unread: 1,
                        file_id: updateOtherUserFiletDto.file_id
                    }
                ];
                await this.notifyRepo.save(notify_data_arr);
            }
            else {
                let notify_data_arr = [{
                        send_to: shared_user.initiator_id,
                        send_by: null,
                        status: 2,
                        is_unread: 1,
                        file_id: updateOtherUserFiletDto.file_id,
                        message: `${updateOtherUserFiletDto.name}, Other user updated signed on file`
                    }];
                result.status = file_status_enums_1.FileStatus.Completed;
                await result.save();
                await this.notifyRepo.save(notify_data_arr);
            }
        }
        else {
            throw new common_1.NotFoundException('No User Found');
        }
        return {
            message: 'File has been updated successfully',
        };
    }
    async editReshareFile(updateFileManagementDto, user) {
        const result = await this.fileRepo.findOne({
            id: updateFileManagementDto.file_id
        });
        const shared_users = await this.sharedUserRepo.find({
            file_id: updateFileManagementDto.file_id,
            is_accept: 2
        });
        if (result && shared_users.length) {
            result.fileName = updateFileManagementDto.filename;
            result.is_editable = 0;
            await result.save();
            let notify_data_arr = [];
            shared_users.forEach(element => {
                let data = {
                    send_to: element.user_id,
                    send_by: user.id,
                    status: 7,
                    is_unread: 1,
                    request_sent: 1,
                    file_id: updateFileManagementDto.file_id
                };
                notify_data_arr.push(data);
            });
            await this.notifyRepo.save(notify_data_arr);
        }
        else {
            throw new common_1.NotFoundException('No User Found');
        }
        return {
            message: 'File has been updated successfully',
        };
    }
    async editSharingFile(saveEditFile, user) {
        const result = await this.fileRepo.findOne({
            id: saveEditFile.FileData.file_id
        });
        if (result) {
            result.fileName = saveEditFile.FileData.filename;
            if (!result.is_sharing) {
                result.is_editable = 0;
            }
            await result.save();
            const new_shared_user_arr = [];
            const rm_shared_user_arr = [];
            const rm_shared_other_user_arr = [];
            await saveEditFile.SharedUser.new_user_ids.forEach(async (id, i) => {
                const data = {
                    file_id: saveEditFile.FileData.file_id,
                    user_id: typeof id == 'number' ? id : null,
                    other_user_name: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[1],
                    other_user_email: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[0],
                    initiator_id: user.id,
                    is_shared: 1,
                    is_locked: 1
                };
                new_shared_user_arr.push(data);
            });
            await saveEditFile.SharedUser.rm_user_ids.forEach(async (id, i) => {
                const file_id = saveEditFile.FileData.file_id;
                if (typeof id == 'number') {
                    await this.sharedUserRepo.delete({ file_id, user_id: id });
                }
                if (typeof id != 'number') {
                    const other_user_name = id.split(/(?<=^\S+)\s/)[1];
                    const other_user_email = id.split(/(?<=^\S+)\s/)[0];
                    await this.sharedUserRepo.delete({ file_id, other_user_name, other_user_email });
                }
            });
            await this.sharedUserRepo.save(new_shared_user_arr);
            const shared_users = await this.sharedUserRepo.find({
                relations: ['user'],
                where: {
                    file_id: saveEditFile.FileData.file_id,
                    is_accept: 2
                }
            });
            const notify_data_arr = [];
            await shared_users.forEach(async (shared_user_data, i) => {
                const notify_data = {
                    send_to: shared_user_data.user_id,
                    send_by: user.id,
                    status: 7,
                    is_unread: 1,
                    request_sent: 1,
                    file_id: saveEditFile.FileData.file_id
                };
                notify_data_arr.push(notify_data);
            });
            await this.notifyRepo.save(notify_data_arr);
            await shared_users.forEach(async (shared_user_data, i) => {
                await this.sharedUserRepo.update({ file_id: saveEditFile.FileData.file_id }, {
                    is_accept: 0
                });
            });
        }
        else {
            throw new common_1.NotFoundException('No File Found');
        }
        return {
            message: 'File has been updated successfully',
        };
    }
    async shareFileFromList(shareFileDto, user) {
        await this.fileRepo.update({
            id: shareFileDto.file_id,
        }, {
            startDate: shareFileDto.startDate,
            endDate: shareFileDto.endDate,
            is_sharing: 0,
            status: file_status_enums_1.FileStatus.InProgress,
            is_editable: 0
        });
        await this.commentRepo.upsert({
            id: shareFileDto.comment_id,
            file_id: shareFileDto.file_id,
            user_id: user.id,
            comment: shareFileDto.comment
        }, { conflictPaths: ['id'] });
        const shared_users = await this.sharedUserRepo.find({
            where: {
                file_id: shareFileDto.file_id,
            }
        });
        const notify_data_arr = [];
        await shared_users.forEach(async (shared_user_data, i) => {
            const notify_data = {
                send_to: shared_user_data.user_id,
                send_by: user.id,
                status: 1,
                is_unread: 1,
                request_sent: 1,
                file_id: shareFileDto.file_id
            };
            notify_data_arr.push(notify_data);
            if (!shared_user_data.id) {
                await this.mailerService
                    .sendMail({
                    to: shared_user_data.other_user_email,
                    subject: 'File Shared for Sign',
                    template: `src/templates/other-signer-user.pug`,
                    context: {
                        name: shared_user_data.other_user_name,
                        email: shared_user_data.other_user_email,
                        start_date: shareFileDto.startDate,
                        end_date: shareFileDto.endDate,
                        url: `https://dms.miraclechd.co/other-user?id=${shared_user_data.id}&name=${shared_user_data.other_user_name}&email=${shared_user_data.other_user_email}`
                    },
                })
                    .then(() => console.log('success'))
                    .catch((e) => console.log(e));
            }
        });
        await this.notifyRepo.save(notify_data_arr);
        return shared_users;
    }
    findAll() {
        return this.fileRepo.find();
    }
    async getFiles(user, page, limit) {
        limit = parseInt(limit) || 10;
        page = parseInt(page) - 1 || 0;
        if (user.role != 'User') {
            const [result, count] = await this.fileRepo.findAndCount({
                relations: ["comment"],
                take: limit,
                skip: (page >= 0 ? page : 0) * limit,
                order: {
                    createdAt: "DESC",
                },
            });
            return {
                list: result,
                total: count,
            };
        }
        else {
            const [result, count] = await this.fileRepo.findAndCount({
                take: limit,
                where: {
                    initiator_id: user.id,
                },
                relations: ["comment"],
                skip: (page >= 0 ? page : 0) * limit,
                order: {
                    createdAt: "DESC",
                },
            });
            return {
                list: result,
                total: count,
            };
        }
    }
    async getSharingFiles(user, page, limit) {
        limit = parseInt(limit) || 10;
        page = parseInt(page) - 1 || 0;
        if (user.role == 'Admin') {
            const [result, count] = await this.fileRepo.findAndCount({
                take: limit,
                where: {
                    is_sharing: 1
                },
                relations: ["comment"],
                skip: (page >= 0 ? page : 0) * limit,
                order: {
                    createdAt: "DESC",
                },
            });
            return {
                list: result,
                total: count,
            };
        }
    }
    async getSharedWithMeFiles(user, page, limit) {
        limit = parseInt(limit) || 10;
        page = parseInt(page) - 1 || 0;
        if (user.role != 'User') {
            const [result, count] = await this.fileRepo.findAndCount();
            return {
                list: result,
                total: count,
            };
        }
        else {
            const [result, count] = await this.sharedUserRepo.findAndCount({
                take: limit,
                where: {
                    file: {
                        is_sharing: 0
                    },
                    user_id: user.id,
                    is_shared: 1
                },
                relations: ["file"],
                skip: (page >= 0 ? page : 0) * limit,
                order: {
                    updatedAt: "DESC",
                }
            });
            return {
                list: result,
                total: count,
            };
        }
    }
    async getSharedFile(id, user) {
        const shared_file = await this.fileRepo.findOne({
            where: {
                id
            }
        });
        const shared_users = await this.sharedUserRepo.find({
            where: {
                file_id: id,
            },
            relations: ["user"]
        });
        const comments = await this.commentRepo.find({
            where: {
                file_id: id,
            },
            relations: ["user", 'file']
        });
        return { shared_users, comments, shared_file };
    }
    async getSharedWithMeFile(id, user) {
        const shared_file = await this.sharedUserRepo.find({
            where: {
                file_id: id,
                user_id: user.id,
                is_shared: 1
            },
            relations: ["user", 'file']
        });
        return { shared_file };
    }
    async getOhterUserFile(data) {
        return await this.sharedUserRepo.findOne({
            where: {
                file_id: data.file_id,
                other_user_email: data.email,
                other_user_name: data.name
            },
            relations: ['file']
        });
    }
    async getInitiatorFileDetails(id, user) {
        const shared_file = await this.sharedUserRepo.find({
            where: {
                file_id: id,
                initiator_id: user.id,
            },
            relations: ["user", 'file']
        });
        return { shared_file };
    }
    async findOne(id, signService) {
        try {
            const file = await this.fileRepo.findOne(id);
            if (!file) {
                throw new common_1.NotFoundException('No File Found');
            }
            const signatures = await signService.getFileSignatures(id);
            return Object.assign(Object.assign({}, file), { signatures });
        }
        catch (error) {
            throw new common_1.NotFoundException('No File Found');
        }
    }
    async getfile(fileId) {
        try {
            const file = await this.fileRepo.findOne(fileId);
            if (!file) {
                throw new common_1.NotFoundException('No File Found');
            }
            return file;
        }
        catch (error) {
            throw new common_1.NotFoundException('No File Found');
        }
    }
    async updateFileStatus(updateFileManagementDto, user) {
        const file = await this.fileRepo.update({ id: updateFileManagementDto.file_id }, {
            status: updateFileManagementDto.status == 'approved' ? file_status_enums_1.FileStatus.Approved
                : (updateFileManagementDto.status == 'completed' ? file_status_enums_1.FileStatus.Completed : (updateFileManagementDto.status == 'inactive' ? file_status_enums_1.FileStatus.Inactive : (updateFileManagementDto.status == 'inprogress' ? file_status_enums_1.FileStatus.InProgress : file_status_enums_1.FileStatus.Pending)))
        });
        const shared_users = await this.sharedUserRepo.find({
            where: {
                file_id: updateFileManagementDto.file_id,
            }
        });
        console.log('user', user);
        const notify_data_arr = [];
        await shared_users.forEach(async (shared_user_data, i) => {
            const notify_data = {
                send_to: shared_user_data.user_id,
                send_by: user.id,
                status: 9,
                is_unread: 1,
                message: updateFileManagementDto.status,
                file_id: updateFileManagementDto.file_id
            };
            notify_data_arr.push(notify_data);
        });
        await this.notifyRepo.save(notify_data_arr);
        return file;
    }
    async shareFile(id, shareFileDto, user) {
        const { comment, startDate, endDate } = shareFileDto;
        const file = await this.getfile(id);
        file.startDate = startDate;
        file.endDate = endDate;
        await file.save();
        return file;
    }
    async updateFileRequestStatus(request, user) {
        const shared_user = await this.sharedUserRepo.findOne({
            file_id: request.file_id,
            user_id: user.id
        });
        const file = await this.fileRepo.findOne({
            id: request.file_id,
        });
        if (file) {
            file.is_editable = request.status == 2 ? 1 : 0;
            await file.save();
        }
        if (shared_user) {
            shared_user.is_accept = request.status;
            await shared_user.save();
        }
        await this.notifyRepo.save([{
                send_to: request.initiator_id,
                send_by: user.id,
                status: request.status == 1 ? 4 : 5,
                is_unread: 1,
                file_id: request.file_id,
                request_sent: 1,
                message: request.message
            }]);
        await this.notifyRepo.update({ id: request.notification_id }, {
            request_sent: 0
        });
        return file;
    }
    remove(id) {
        return `This action removes a #${id} fileManagement`;
    }
    async search(SearchFileRequestDto, limit, page, user) {
        console.log('user id', user.id);
        const keyword = SearchFileRequestDto.keyword;
        const where = {
            originalFileName: (0, typeorm_2.Like)(`%${keyword}%`),
            initiator: {
                role: SearchFileRequestDto.role ? SearchFileRequestDto.role : (0, typeorm_2.In)([user_enums_1.UserRole.Admin, user_enums_1.UserRole.User]),
            },
        };
        if (SearchFileRequestDto.date) {
            where.startDate = (0, typeorm_2.Like)(`%${SearchFileRequestDto.date}%`);
        }
        if (SearchFileRequestDto.status) {
            where.status = SearchFileRequestDto.status;
        }
        if (user.role != user_enums_1.UserRole.Admin) {
            where.initiator.id = user.id;
        }
        const [result, count] = await this.fileRepo.findAndCount({
            relations: ['initiator'],
            take: limit,
            where: where,
            skip: (page >= 0 ? page : 0) * limit,
        });
        return {
            list: result,
            total: count,
        };
    }
    async searchSharedWithMe(SearchFileRequestDto, limit, page, user) {
        console.log('user id', user.id);
        const keyword = SearchFileRequestDto.keyword;
        const shared_user_files = await this.sharedUserRepo.find({
            select: ["file_id"],
            where: {
                file: {
                    is_sharing: 0
                },
                user_id: user.id,
                is_shared: 1
            },
            relations: ["file"],
        });
        let shared_files_id_arr = [];
        shared_user_files.forEach(element => {
            shared_files_id_arr.push(element.file_id);
        });
        const where = {
            originalFileName: (0, typeorm_2.Like)(`%${keyword}%`),
            id: (0, typeorm_2.In)(shared_files_id_arr),
            initiator: {
                role: SearchFileRequestDto.role ? SearchFileRequestDto.role : (0, typeorm_2.In)([user_enums_1.UserRole.Admin, user_enums_1.UserRole.User]),
            },
        };
        if (SearchFileRequestDto.date) {
            where.startDate = (0, typeorm_2.Like)(`%${SearchFileRequestDto.date}%`);
        }
        if (SearchFileRequestDto.status) {
            where.status = SearchFileRequestDto.status;
        }
        const [result, count] = await this.fileRepo.findAndCount({
            relations: ['initiator'],
            take: limit,
            where: where,
            skip: (page >= 0 ? page : 0) * limit,
        });
        return {
            list: result,
            total: count,
        };
    }
    async searchSharingFile(SearchFileRequestDto, limit, page, user) {
        const keyword = SearchFileRequestDto.keyword;
        const where = {
            originalFileName: (0, typeorm_2.Like)(`%${keyword}%`),
            is_sharing: 1,
            initiator: {
                role: SearchFileRequestDto.role ? SearchFileRequestDto.role : (0, typeorm_2.In)([user_enums_1.UserRole.Admin, user_enums_1.UserRole.User]),
            },
        };
        if (SearchFileRequestDto.date) {
            where.startDate = (0, typeorm_2.Like)(`%${SearchFileRequestDto.date}%`);
        }
        if (SearchFileRequestDto.status) {
            where.status = SearchFileRequestDto.status;
        }
        const [result, count] = await this.fileRepo.findAndCount({
            relations: ['initiator'],
            take: limit,
            where: where,
            skip: (page >= 0 ? page : 0) * limit,
        });
        return {
            list: result,
            total: count,
        };
    }
    async deleteFile(id, user) {
        const deleteResponse = await this.fileRepo.softDelete(id);
        if (!deleteResponse.affected) {
            throw new common_1.NotFoundException(id);
        }
        else {
            const deleteResponse = await this.fileRepo.update({
                id
            }, {
                status: file_status_enums_1.FileStatus.Deleted
            });
        }
    }
};
FileManagementService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(file_management_repository_1.FileManagementRepository)),
    __param(1, (0, typeorm_1.InjectRepository)(user_shared_files_repositroy_1.SharedUserRepository)),
    __param(2, (0, typeorm_1.InjectRepository)(file_comment_repositroy_1.CommentRepository)),
    __param(3, (0, typeorm_1.InjectRepository)(notifications_repository_1.NotificationRepository)),
    __metadata("design:paramtypes", [file_management_repository_1.FileManagementRepository,
        user_shared_files_repositroy_1.SharedUserRepository,
        file_comment_repositroy_1.CommentRepository,
        notifications_repository_1.NotificationRepository,
        mailer_1.MailerService])
], FileManagementService);
exports.FileManagementService = FileManagementService;
//# sourceMappingURL=file-management.service.js.map