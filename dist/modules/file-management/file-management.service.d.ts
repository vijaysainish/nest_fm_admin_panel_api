import { User } from '../users/entities/user.entity';
import { SignatureService } from './../signature/signature.service';
import { CreateFileManagementDto } from './dto/create-file-management.dto';
import { ShareFileDto } from './dto/share-file.dto';
import { UpdateFileManagementDto } from './dto/update-file-management.dto';
import { FileManagementRepository } from './repository/file-management.repository';
import { SharedUserRepository } from './repository/user-shared-files.repositroy';
import { CommentRepository } from './repository/file-comment.repositroy';
import { NotificationRepository } from '../notifications/repository/notifications.repository';
import { SharedUser } from './entities/user-shared-files.entity';
import { FileStatus } from 'src/enums/file-status.enums';
import { UpdateFileRequestDto } from './dto/update-file-request.dto copy';
import { SearchFileRequestDto } from './dto/search.dto';
import { MailerService } from '@nestjs-modules/mailer';
import { UpdateOtherUserFiletDto } from './dto/update-other-user-file.dto';
import { saveEditFileDto } from './dto/save-edit-file.dto';
export declare class FileManagementService {
    private fileRepo;
    private sharedUserRepo;
    private commentRepo;
    private notifyRepo;
    private readonly mailerService;
    constructor(fileRepo: FileManagementRepository, sharedUserRepo: SharedUserRepository, commentRepo: CommentRepository, notifyRepo: NotificationRepository, mailerService: MailerService);
    create(createFileManagementDto: CreateFileManagementDto, user: User): Promise<import("./entities/file-management.entity").ShareFiles>;
    saveFile(createFileManagementDto: CreateFileManagementDto, user: User): Promise<import("./entities/file-management.entity").ShareFiles>;
    updateFile(updateFileManagementDto: UpdateFileManagementDto, user: User): Promise<{
        message: string;
    }>;
    updateOtherUserFile(updateOtherUserFiletDto: UpdateOtherUserFiletDto): Promise<{
        message: string;
    }>;
    editReshareFile(updateFileManagementDto: UpdateFileManagementDto, user: User): Promise<{
        message: string;
    }>;
    editSharingFile(saveEditFile: saveEditFileDto, user: User): Promise<{
        message: string;
    }>;
    shareFileFromList(shareFileDto: ShareFileDto, user: User): Promise<SharedUser[]>;
    findAll(): Promise<import("./entities/file-management.entity").ShareFiles[]>;
    getFiles(user: User, page: any, limit: any): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    }>;
    getSharingFiles(user: User, page: any, limit: any): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    }>;
    getSharedWithMeFiles(user: User, page: any, limit: any): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    } | {
        list: SharedUser[];
        total: number;
    }>;
    getSharedFile(id: any, user: User): Promise<{
        shared_users: any;
        comments: any;
        shared_file: any;
    }>;
    getSharedWithMeFile(id: any, user: User): Promise<{
        shared_file: any;
    }>;
    getOhterUserFile(data: any): Promise<SharedUser>;
    getInitiatorFileDetails(id: any, user: User): Promise<{
        shared_file: any;
    }>;
    findOne(id: string, signService: SignatureService): Promise<{
        signatures: import("../signature/entities/signature.entity").Signature[];
        id: number;
        initiator_id: number;
        fileName: string;
        originalFileName: string;
        startDate: Date;
        endDate: Date;
        status: FileStatus;
        is_sharing: number;
        is_editable: number;
        createdAt: Date;
        updatedAt: Date;
        deletedAt: Date;
        initiator: User[];
        comment: import("./entities/file-comment.entity").FileComment[];
        shared_users: SharedUser[];
    }>;
    getfile(fileId: string): Promise<import("./entities/file-management.entity").ShareFiles>;
    updateFileStatus(updateFileManagementDto: UpdateFileManagementDto, user: User): Promise<import("typeorm").UpdateResult>;
    shareFile(id: string, shareFileDto: ShareFileDto, user: User): Promise<import("./entities/file-management.entity").ShareFiles>;
    updateFileRequestStatus(request: UpdateFileRequestDto, user: User): Promise<import("./entities/file-management.entity").ShareFiles>;
    remove(id: string): string;
    search(SearchFileRequestDto: SearchFileRequestDto, limit: any, page: any, user: User): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    }>;
    searchSharedWithMe(SearchFileRequestDto: SearchFileRequestDto, limit: any, page: any, user: User): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    }>;
    searchSharingFile(SearchFileRequestDto: SearchFileRequestDto, limit: any, page: any, user: User): Promise<{
        list: import("./entities/file-management.entity").ShareFiles[];
        total: number;
    }>;
    deleteFile(id: number, user: User): Promise<void>;
}
