import { BaseEntity } from 'typeorm';
import { User } from './../../users/entities/user.entity';
import { ShareFiles } from './../../file-management/entities/file-management.entity';
export declare class FileComment extends BaseEntity {
    id: number;
    file_id: number;
    user_id: number;
    comment: string;
    createdAt: Date;
    user: User;
    file: ShareFiles;
}
