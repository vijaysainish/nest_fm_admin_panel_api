"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShareFiles = void 0;
const user_entity_1 = require("../../users/entities/user.entity");
const typeorm_1 = require("typeorm");
const file_status_enums_1 = require("./../../../enums/file-status.enums");
const file_comment_entity_1 = require("./file-comment.entity");
const user_shared_files_entity_1 = require("./user-shared-files.entity");
let ShareFiles = class ShareFiles extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], ShareFiles.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], ShareFiles.prototype, "initiator_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ShareFiles.prototype, "fileName", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ShareFiles.prototype, "originalFileName", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", Date)
], ShareFiles.prototype, "startDate", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", Date)
], ShareFiles.prototype, "endDate", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'enum', enum: file_status_enums_1.FileStatus, default: file_status_enums_1.FileStatus.Pending }),
    __metadata("design:type", String)
], ShareFiles.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", Number)
], ShareFiles.prototype, "is_sharing", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: 0 }),
    __metadata("design:type", Number)
], ShareFiles.prototype, "is_editable", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], ShareFiles.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], ShareFiles.prototype, "updatedAt", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)({ nullable: true }),
    __metadata("design:type", Date)
], ShareFiles.prototype, "deletedAt", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => user_entity_1.User, user => user.id, {
        onDelete: "CASCADE"
    }),
    (0, typeorm_1.JoinColumn)({ name: "initiator_id" }),
    __metadata("design:type", Array)
], ShareFiles.prototype, "initiator", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => file_comment_entity_1.FileComment, comment => comment.file, {
        onDelete: "CASCADE"
    }),
    (0, typeorm_1.JoinColumn)({ name: "id" }),
    __metadata("design:type", Array)
], ShareFiles.prototype, "comment", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => user_shared_files_entity_1.SharedUser, shared_user => shared_user.file, {
        onDelete: "CASCADE"
    }),
    (0, typeorm_1.JoinColumn)({ name: "id" }),
    __metadata("design:type", Array)
], ShareFiles.prototype, "shared_users", void 0);
ShareFiles = __decorate([
    (0, typeorm_1.Entity)()
], ShareFiles);
exports.ShareFiles = ShareFiles;
//# sourceMappingURL=file-management.entity.js.map