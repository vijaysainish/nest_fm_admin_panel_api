"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SharedUser = void 0;
const typeorm_1 = require("typeorm");
const file_status_enums_1 = require("./../../../enums/file-status.enums");
const user_entity_1 = require("./../../users/entities/user.entity");
const file_management_entity_1 = require("./../../file-management/entities/file-management.entity");
let SharedUser = class SharedUser extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], SharedUser.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], SharedUser.prototype, "file_id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], SharedUser.prototype, "initiator_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", Number)
], SharedUser.prototype, "user_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", String)
], SharedUser.prototype, "other_user_name", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", String)
], SharedUser.prototype, "other_user_email", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'enum', enum: file_status_enums_1.FileStatus, default: file_status_enums_1.FileStatus.Pending }),
    __metadata("design:type", String)
], SharedUser.prototype, "sign_status", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: 0 }),
    __metadata("design:type", Number)
], SharedUser.prototype, "is_shared", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: 0, comment: '1 for accepted, 2 for rejected' }),
    __metadata("design:type", Number)
], SharedUser.prototype, "is_accept", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: 0 }),
    __metadata("design:type", Number)
], SharedUser.prototype, "is_locked", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], SharedUser.prototype, "updatedAt", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => user_entity_1.User, user => user.id, {
        onDelete: "CASCADE"
    }),
    (0, typeorm_1.JoinColumn)({ name: "user_id" }),
    __metadata("design:type", user_entity_1.User)
], SharedUser.prototype, "user", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => user_entity_1.User, user => user.id, {
        onDelete: "CASCADE"
    }),
    (0, typeorm_1.JoinColumn)({ name: "initiator_id" }),
    __metadata("design:type", user_entity_1.User)
], SharedUser.prototype, "initiator", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => file_management_entity_1.ShareFiles, file => file.id, {
        onDelete: "CASCADE"
    }),
    (0, typeorm_1.JoinColumn)({ name: "file_id" }),
    __metadata("design:type", file_management_entity_1.ShareFiles)
], SharedUser.prototype, "file", void 0);
SharedUser = __decorate([
    (0, typeorm_1.Entity)()
], SharedUser);
exports.SharedUser = SharedUser;
//# sourceMappingURL=user-shared-files.entity.js.map