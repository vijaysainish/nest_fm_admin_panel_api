import { BaseEntity } from 'typeorm';
import { FileStatus } from './../../../enums/file-status.enums';
import { User } from './../../users/entities/user.entity';
import { ShareFiles } from './../../file-management/entities/file-management.entity';
export declare class SharedUser extends BaseEntity {
    id: number;
    file_id: number;
    initiator_id: number;
    user_id: number;
    other_user_name: string;
    other_user_email: string;
    sign_status: FileStatus;
    is_shared: number;
    is_accept: number;
    is_locked: number;
    updatedAt: Date;
    user: User;
    initiator: User;
    file: ShareFiles;
}
