import { User } from 'src/modules/users/entities/user.entity';
import { BaseEntity } from 'typeorm';
import { FileStatus } from './../../../enums/file-status.enums';
import { FileComment } from './file-comment.entity';
import { SharedUser } from './user-shared-files.entity';
export declare class ShareFiles extends BaseEntity {
    id: number;
    initiator_id: number;
    fileName: string;
    originalFileName: string;
    startDate: Date;
    endDate: Date;
    status: FileStatus;
    is_sharing: number;
    is_editable: number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    initiator: User[];
    comment: FileComment[];
    shared_users: SharedUser[];
}
