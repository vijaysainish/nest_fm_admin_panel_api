"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileManagementController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const multer_1 = require("multer");
const getUser_decorator_1 = require("../../decorators/getUser.decorator");
const auth_guard_1 = require("../auth/auth.guard");
const signature_service_1 = require("../signature/signature.service");
const user_entity_1 = require("../users/entities/user.entity");
const create_file_management_dto_1 = require("./dto/create-file-management.dto");
const share_file_dto_1 = require("./dto/share-file.dto");
const update_file_management_dto_1 = require("./dto/update-file-management.dto");
const file_management_service_1 = require("./file-management.service");
const role_guard_1 = require("../auth/role.guard");
const update_file_request_dto_copy_1 = require("./dto/update-file-request.dto copy");
const search_dto_1 = require("./dto/search.dto");
const update_other_user_file_dto_1 = require("./dto/update-other-user-file.dto");
const save_edit_file_dto_1 = require("./dto/save-edit-file.dto");
let FileManagementController = class FileManagementController {
    constructor(fileManagementService, signService) {
        this.fileManagementService = fileManagementService;
        this.signService = signService;
    }
    create(createFileManagementDto, user) {
        return this.fileManagementService.create(createFileManagementDto, user);
    }
    getFiles(user, page, limit) {
        return this.fileManagementService.getFiles(user, page, limit);
    }
    getSharingFiles(user, page, limit) {
        return this.fileManagementService.getSharingFiles(user, page, limit);
    }
    getSharedWithMeFiles(user, page, limit) {
        return this.fileManagementService.getSharedWithMeFiles(user, page, limit);
    }
    getSharedFile(id, user) {
        return this.fileManagementService.getSharedFile(id, user);
    }
    getSharedWithMeFile(id, user) {
        return this.fileManagementService.getSharedWithMeFile(id, user);
    }
    getOhterUserFile(name, email, file_id) {
        const data = {
            file_id, name, email
        };
        return this.fileManagementService.getOhterUserFile(data);
    }
    getInitiatorFileDetails(id, user) {
        return this.fileManagementService.getInitiatorFileDetails(id, user);
    }
    findOne(id) {
        return this.fileManagementService.findOne(id, this.signService);
    }
    sharefile(id, shareFileDto, user) {
        return this.fileManagementService.shareFile(id, shareFileDto, user);
    }
    acceptRejectFileRequest(UpdateRequestDto, user) {
        return this.fileManagementService.updateFileRequestStatus(UpdateRequestDto, user);
    }
    updateFileStatus(updateFileManagementDto, user) {
        return this.fileManagementService.updateFileStatus(updateFileManagementDto, user);
    }
    uploadFile(file) {
        return file;
    }
    addFile(filedata, user) {
        return this.fileManagementService.create(filedata, user);
    }
    saveFile(filedata, user) {
        return this.fileManagementService.saveFile(filedata, user);
    }
    search(searchdata, user) {
        return this.fileManagementService.search(searchdata, 10, 0, user);
    }
    searchSharedWithMe(searchdata, user) {
        return this.fileManagementService.searchSharedWithMe(searchdata, 10, 0, user);
    }
    searchSharingFile(searchdata, user) {
        return this.fileManagementService.searchSharingFile(searchdata, 10, 0, user);
    }
    updateFile(filedata, user) {
        return this.fileManagementService.updateFile(filedata, user);
    }
    updateOtherUserFile(filedata) {
        return this.fileManagementService.updateOtherUserFile(filedata);
    }
    editReshareFile(filedata, user) {
        return this.fileManagementService.editReshareFile(filedata, user);
    }
    editSharingFile(filedata, user) {
        return this.fileManagementService.editSharingFile(filedata, user);
    }
    shareFileFromList(filedata, user) {
        return this.fileManagementService.shareFileFromList(filedata, user);
    }
    deleteFile(data, user) {
        return this.fileManagementService.deleteFile(data.file_id, user);
    }
    seeUploadedFile(image, res) {
        return res.sendFile(image, { root: './uploads' });
    }
};
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_file_management_dto_1.CreateFileManagementDto,
        user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "create", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Get)('files-list'),
    __param(0, (0, getUser_decorator_1.GetUser)()),
    __param(1, (0, common_1.Query)('page')),
    __param(2, (0, common_1.Query)('limit')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User, Object, Object]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "getFiles", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Get)('sharing-files'),
    __param(0, (0, getUser_decorator_1.GetUser)()),
    __param(1, (0, common_1.Query)('page')),
    __param(2, (0, common_1.Query)('limit')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User, Object, Object]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "getSharingFiles", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Get)('files-shared-with-me'),
    __param(0, (0, getUser_decorator_1.GetUser)()),
    __param(1, (0, common_1.Query)('page')),
    __param(2, (0, common_1.Query)('limit')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User, Object, Object]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "getSharedWithMeFiles", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Get)('shared-file'),
    __param(0, (0, common_1.Query)('id')),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "getSharedFile", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Get)('shared-with-me-file'),
    __param(0, (0, common_1.Query)('id')),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "getSharedWithMeFile", null);
__decorate([
    (0, common_1.Get)('other-user-file'),
    __param(0, (0, common_1.Query)('name')),
    __param(1, (0, common_1.Query)('email')),
    __param(2, (0, common_1.Query)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "getOhterUserFile", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Get)('initiator-file-details'),
    __param(0, (0, common_1.Query)('id')),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "getInitiatorFileDetails", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "findOne", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('share/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, share_file_dto_1.ShareFileDto,
        user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "sharefile", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('update-file-request-status'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_file_request_dto_copy_1.UpdateFileRequestDto,
        user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "acceptRejectFileRequest", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Patch)('update-file-status'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_file_management_dto_1.UpdateFileManagementDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "updateFileStatus", null);
__decorate([
    (0, common_1.Post)('upload'),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file', {
        storage: (0, multer_1.diskStorage)({
            destination: './uploads',
        }),
    })),
    __param(0, (0, common_1.UploadedFile)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "uploadFile", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('add-file'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_file_management_dto_1.CreateFileManagementDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "addFile", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('save-file'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_file_management_dto_1.CreateFileManagementDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "saveFile", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('search-listing-file'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [search_dto_1.SearchFileRequestDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "search", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('search-shared-with-me'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [search_dto_1.SearchFileRequestDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "searchSharedWithMe", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('search-sharing-file'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [search_dto_1.SearchFileRequestDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "searchSharingFile", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('update-file'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_file_management_dto_1.UpdateFileManagementDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "updateFile", null);
__decorate([
    (0, common_1.Post)('update-other-user-file'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_other_user_file_dto_1.UpdateOtherUserFiletDto]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "updateOtherUserFile", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('edit-reshare-file'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_file_management_dto_1.UpdateFileManagementDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "editReshareFile", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('edit-sharing-file'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [save_edit_file_dto_1.saveEditFileDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "editSharingFile", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Post)('share-file'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [share_file_dto_1.ShareFileDto, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "shareFileFromList", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.JwtAuthGuard, role_guard_1.RoleGuard),
    (0, common_1.Delete)('delete-file'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, getUser_decorator_1.GetUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "deleteFile", null);
__decorate([
    (0, common_1.Get)('file/:imgpath'),
    __param(0, (0, common_1.Param)('imgpath')),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], FileManagementController.prototype, "seeUploadedFile", null);
FileManagementController = __decorate([
    (0, common_1.Controller)('file-management'),
    __metadata("design:paramtypes", [file_management_service_1.FileManagementService,
        signature_service_1.SignatureService])
], FileManagementController);
exports.FileManagementController = FileManagementController;
//# sourceMappingURL=file-management.controller.js.map