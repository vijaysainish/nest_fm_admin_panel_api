import { CreateFileManagementDto } from './create-file-management.dto';
declare const UpdateOtherUserFiletDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateFileManagementDto>>;
export declare class UpdateOtherUserFiletDto extends UpdateOtherUserFiletDto_base {
    filename: string;
    status: string;
    file_id: number;
    name: string;
    email: string;
}
export {};
