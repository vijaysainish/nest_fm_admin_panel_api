export declare class ShareFileDto {
    file_id: number;
    comment: string;
    comment_id: number;
    startDate: Date;
    endDate: Date;
}
