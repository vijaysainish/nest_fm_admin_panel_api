import { CreateFileManagementDto } from './create-file-management.dto';
declare const UpdateFileRequestDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateFileManagementDto>>;
export declare class UpdateFileRequestDto extends UpdateFileRequestDto_base {
    notification_id: number;
    file_id: number;
    status: number;
    initiator_id: number;
    message: string;
}
export {};
