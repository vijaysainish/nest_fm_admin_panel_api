declare class SharedUser {
    user_ids: Array<any>;
}
declare class FileData {
    filename: string;
    originalFileName: string;
    startDate: Date;
    endDate: Date;
    comment: any;
}
export declare class CreateFileManagementDto {
    readonly SharedUser: SharedUser;
    readonly FileData: FileData;
}
export {};
