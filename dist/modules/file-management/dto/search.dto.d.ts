export declare class SearchFileRequestDto {
    keyword: string;
    role: string;
    status: string;
    date: Date;
    start_date: Date;
    end_date: Date;
}
