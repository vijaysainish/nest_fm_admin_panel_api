declare class SharedUser {
    new_user_ids: Array<any>;
    rm_user_ids: Array<any>;
}
declare class FileData {
    filename: string;
    file_id: number;
}
export declare class saveEditFileDto {
    readonly SharedUser: SharedUser;
    readonly FileData: FileData;
}
export {};
