import { CreateFileManagementDto } from './create-file-management.dto';
declare const UpdateFileManagementDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateFileManagementDto>>;
export declare class UpdateFileManagementDto extends UpdateFileManagementDto_base {
    filename: string;
    status: string;
    file_id: number;
}
export {};
