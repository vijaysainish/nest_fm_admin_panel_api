import { NotificationsService } from './notifications.service';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { UpdateNotificationDto } from './dto/update-notification.dto';
import { User } from '../users/entities/user.entity';
export declare class NotificationsController {
    private readonly notificationsService;
    constructor(notificationsService: NotificationsService);
    create(createNotificationDto: CreateNotificationDto): string;
    findAll(): string;
    getUserNotification(user: User, page: any, limit: any): Promise<{
        list: import("./entities/notification.entity").Notification[];
        total: number;
        unread_msg: number;
    }>;
    findOne(id: string): string;
    readNotifications(user: User): Promise<import("typeorm").UpdateResult>;
    update(id: string, updateNotificationDto: UpdateNotificationDto): string;
    remove(id: string): string;
}
