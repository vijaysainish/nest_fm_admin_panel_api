import { ShareFiles } from 'src/modules/file-management/entities/file-management.entity';
import { User } from 'src/modules/users/entities/user.entity';
import { BaseEntity } from 'typeorm';
export declare class Notification extends BaseEntity {
    id: any;
    file_id: number;
    send_by: number;
    send_to: number;
    status: number;
    request_sent: number;
    message: string;
    is_unread: number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    send_by_user: User[];
    send_to_user: User[];
    shared_file: ShareFiles[];
}
