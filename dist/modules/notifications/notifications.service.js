"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const notifications_repository_1 = require("./repository/notifications.repository");
let NotificationsService = class NotificationsService {
    constructor(notifyRepo) {
        this.notifyRepo = notifyRepo;
    }
    create(createNotificationDto) {
        return 'This action adds a new notification';
    }
    findAll() {
        return `This action returns all notifications`;
    }
    async getUserNotifications(user, limit, page) {
        console.log(user);
        limit = parseInt(limit) || 10;
        page = parseInt(page) - 1 || 0;
        const [result, count] = await this.notifyRepo.findAndCount({
            where: {
                send_to: user.id
            },
            order: {
                createdAt: "DESC",
            },
            relations: ["send_by_user", 'send_to_user', 'shared_file'],
            take: limit,
            skip: (page >= 0 ? page : 0) * limit,
        });
        const unread_msg = await this.notifyRepo.count({
            where: {
                send_to: user.id,
                is_unread: 1
            }
        });
        return {
            list: result,
            total: count,
            unread_msg
        };
    }
    findOne(id) {
        return `This action returns a #${id} notification`;
    }
    async readNotifications(user) {
        const notify_users = await this.notifyRepo.update({ send_to: user.id }, {
            is_unread: 0
        });
        return notify_users;
    }
    update(id, updateNotificationDto) {
        return `This action updates a #${id} notification`;
    }
    remove(id) {
        return `This action removes a #${id} notification`;
    }
};
NotificationsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(notifications_repository_1.NotificationRepository)),
    __metadata("design:paramtypes", [notifications_repository_1.NotificationRepository])
], NotificationsService);
exports.NotificationsService = NotificationsService;
//# sourceMappingURL=notifications.service.js.map