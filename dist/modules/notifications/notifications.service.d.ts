import { User } from '../users/entities/user.entity';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { UpdateNotificationDto } from './dto/update-notification.dto';
import { NotificationRepository } from './repository/notifications.repository';
export declare class NotificationsService {
    private notifyRepo;
    constructor(notifyRepo: NotificationRepository);
    create(createNotificationDto: CreateNotificationDto): string;
    findAll(): string;
    getUserNotifications(user: User, limit: any, page: any): Promise<{
        list: import("./entities/notification.entity").Notification[];
        total: number;
        unread_msg: number;
    }>;
    findOne(id: number): string;
    readNotifications(user: User): Promise<import("typeorm").UpdateResult>;
    update(id: number, updateNotificationDto: UpdateNotificationDto): string;
    remove(id: number): string;
}
