import { CreateLogoDto } from './dto/logo.dto';
import { LogoRepository } from './repository/logo.repository';
export declare class AdminService {
    private logoRepo;
    constructor(logoRepo: LogoRepository);
    getLogo(): Promise<import("./entities/logo.entity").Logo>;
    updateLogoInfo(createLogoDto: CreateLogoDto): Promise<import("typeorm").UpdateResult>;
}
