"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogoRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const logo_entity_1 = require("./../entities/logo.entity");
let LogoRepository = class LogoRepository extends typeorm_1.Repository {
    async createLogo(createLogoDto) {
        const data = createLogoDto;
        const logo = new logo_entity_1.Logo();
        logo.name = data.name;
        logo.image = data.image;
        try {
            await logo.save();
            return logo;
        }
        catch (e) {
            console.log(e);
            throw new common_1.InternalServerErrorException('Something Went wrong');
        }
    }
};
LogoRepository = __decorate([
    (0, typeorm_1.EntityRepository)(logo_entity_1.Logo)
], LogoRepository);
exports.LogoRepository = LogoRepository;
//# sourceMappingURL=logo.repository.js.map