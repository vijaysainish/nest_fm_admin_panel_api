import { Repository } from 'typeorm';
import { CreateLogoDto } from '../dto/logo.dto';
import { Logo } from './../entities/logo.entity';
export declare class LogoRepository extends Repository<Logo> {
    createLogo(createLogoDto: CreateLogoDto): Promise<Logo>;
}
