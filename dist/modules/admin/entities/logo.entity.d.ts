import { BaseEntity } from 'typeorm';
export declare class Logo extends BaseEntity {
    id: number;
    name: string;
    image: string;
    createdAt: Date;
    updatedAt: Date;
}
