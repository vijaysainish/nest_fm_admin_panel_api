import { AdminService } from './admin.service';
import { CreateLogoDto } from './dto/logo.dto';
export declare class AdminController {
    private readonly adminService;
    constructor(adminService: AdminService);
    getLogo(): Promise<import("./entities/logo.entity").Logo>;
    updateLogoInfo(data: CreateLogoDto): Promise<import("typeorm").UpdateResult>;
}
