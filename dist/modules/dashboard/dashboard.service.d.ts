import { User } from './../users/entities/user.entity';
import { UserRepository } from '../users/repository/user.repository';
import { SharedUserRepository } from '../file-management/repository/user-shared-files.repositroy';
import { Repository } from 'typeorm';
export declare class DashboardService {
    private userRepos;
    private sharedUsers;
    private readonly sharedUserRepository;
    constructor(userRepos: UserRepository, sharedUsers: SharedUserRepository, sharedUserRepository: Repository<SharedUserRepository>);
    findAll(user: User): Promise<{
        totalCustomers: number;
        activeCustomers: number;
        inactiveCustomers: number;
        deactivatedCustomers: number;
        inactiveFiles: number;
        total_files: any;
        active_files: any;
        all_users: any[];
        all_files: any[];
        user_completed_files?: undefined;
        user_pending_files?: undefined;
        user_inprogress_files?: undefined;
        user_inactive_files?: undefined;
        user_deleted_files?: undefined;
        user_files?: undefined;
    } | {
        totalCustomers: number;
        activeCustomers: number;
        inactiveCustomers: number;
        deactivatedCustomers: number;
        user_completed_files: any;
        user_pending_files: any;
        user_inprogress_files: any;
        user_inactive_files: any;
        user_deleted_files: any;
        user_files: any[];
        inactiveFiles?: undefined;
        total_files?: undefined;
        active_files?: undefined;
        all_users?: undefined;
        all_files?: undefined;
    }>;
}
