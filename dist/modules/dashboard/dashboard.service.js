"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DashboardService = void 0;
const user_entity_1 = require("./../users/entities/user.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_repository_1 = require("../users/repository/user.repository");
const user_enums_1 = require("../../enums/user.enums");
const user_shared_files_repositroy_1 = require("../file-management/repository/user-shared-files.repositroy");
const typeorm_2 = require("typeorm");
const user_shared_files_entity_1 = require("../file-management/entities/user-shared-files.entity");
const file_management_entity_1 = require("../file-management/entities/file-management.entity");
const file_status_enums_1 = require("../../enums/file-status.enums");
let DashboardService = class DashboardService {
    constructor(userRepos, sharedUsers, sharedUserRepository) {
        this.userRepos = userRepos;
        this.sharedUsers = sharedUsers;
        this.sharedUserRepository = sharedUserRepository;
    }
    async findAll(user) {
        const total_files = await (0, typeorm_2.getConnection)()
            .createQueryBuilder()
            .select('COUNT(DISTINCT shareduser.file_id)', 'total_files_count')
            .from(user_shared_files_entity_1.SharedUser, 'shareduser')
            .getRawOne();
        const active_files = await (0, typeorm_2.getConnection)()
            .createQueryBuilder()
            .select('COUNT(DISTINCT shareFiles.id)', 'active_files_count')
            .from(user_shared_files_entity_1.SharedUser, 'shareduser')
            .innerJoin(file_management_entity_1.ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
            .andWhere("shareFiles.status = :status", { status: file_status_enums_1.FileStatus.Completed })
            .getRawOne();
        const user_completed_files = await (0, typeorm_2.getConnection)()
            .createQueryBuilder()
            .select('COUNT(DISTINCT shareFiles.id)', 'count')
            .from(user_shared_files_entity_1.SharedUser, 'shareduser')
            .innerJoin(file_management_entity_1.ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
            .where("shareFiles.status = :status", { status: file_status_enums_1.FileStatus.Completed })
            .andWhere(new typeorm_2.Brackets((qb) => {
            qb.where("shareduser.initiator_id = :id", { id: user.id })
                .orWhere("shareduser.user_id = :uid", { uid: user.id });
        }))
            .getRawOne();
        const user_pending_files = await (0, typeorm_2.getConnection)()
            .createQueryBuilder()
            .select('COUNT(DISTINCT shareFiles.id)', 'count')
            .from(user_shared_files_entity_1.SharedUser, 'shareduser')
            .innerJoin(file_management_entity_1.ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
            .where("shareFiles.status = :status", { status: file_status_enums_1.FileStatus.Pending })
            .andWhere(new typeorm_2.Brackets((qb) => {
            qb.where("shareduser.initiator_id = :id", { id: user.id })
                .orWhere("shareduser.user_id = :uid", { uid: user.id });
        }))
            .getRawOne();
        const user_inprogress_files = await (0, typeorm_2.getConnection)()
            .createQueryBuilder()
            .select('COUNT(DISTINCT shareFiles.id)', 'count')
            .from(user_shared_files_entity_1.SharedUser, 'shareduser')
            .innerJoin(file_management_entity_1.ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
            .where("shareFiles.status = :status", { status: file_status_enums_1.FileStatus.InProgress })
            .andWhere(new typeorm_2.Brackets((qb) => {
            qb.where("shareduser.initiator_id = :id", { id: user.id })
                .orWhere("shareduser.user_id = :uid", { uid: user.id });
        }))
            .getRawOne();
        const user_inactive_files = await (0, typeorm_2.getConnection)()
            .createQueryBuilder()
            .select('COUNT(DISTINCT shareFiles.id)', 'count')
            .from(user_shared_files_entity_1.SharedUser, 'shareduser')
            .innerJoin(file_management_entity_1.ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
            .where("shareFiles.status = :status", { status: file_status_enums_1.FileStatus.Inactive })
            .andWhere(new typeorm_2.Brackets((qb) => {
            qb.where("shareduser.initiator_id = :id", { id: user.id })
                .orWhere("shareduser.user_id = :uid", { uid: user.id });
        }))
            .getRawOne();
        const user_deleted_files = await (0, typeorm_2.getConnection)()
            .createQueryBuilder()
            .withDeleted()
            .select('COUNT(DISTINCT shareFiles.id)', 'count')
            .from(user_shared_files_entity_1.SharedUser, 'shareduser')
            .innerJoin(file_management_entity_1.ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
            .where("shareFiles.status = :status", { status: file_status_enums_1.FileStatus.Deleted })
            .andWhere(new typeorm_2.Brackets((qb) => {
            qb.where("shareduser.initiator_id = :id", { id: user.id })
                .orWhere("shareduser.user_id = :uid", { uid: user.id });
        }))
            .getRawOne();
        const user_files = await (0, typeorm_2.getConnection)()
            .createQueryBuilder()
            .select('shareFiles.*')
            .from(user_shared_files_entity_1.SharedUser, 'shareduser')
            .innerJoin(file_management_entity_1.ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
            .andWhere(new typeorm_2.Brackets((qb) => {
            qb.where("shareduser.initiator_id = :id", { id: user.id })
                .orWhere("shareduser.user_id = :uid", { uid: user.id });
        }))
            .groupBy('file_id')
            .limit(10)
            .getRawMany();
        const all_users = await (0, typeorm_2.getConnection)()
            .createQueryBuilder()
            .select('GROUP_CONCAT(isActive) AS status,GROUP_CONCAT(c) count,GROUP_CONCAT(m) month,GROUP_CONCAT(DISTINCT y) year')
            .from((subQuery) => {
            return subQuery
                .select("count(*) c,monthname(createdAt) m,year(createdAt) y,isActive")
                .from(user_entity_1.User, "user")
                .groupBy('year(createdAt),month(createdAt),isActive');
        }, "users")
            .groupBy('y')
            .addGroupBy('m')
            .getRawMany();
        const all_files = await (0, typeorm_2.getConnection)()
            .createQueryBuilder()
            .select('GROUP_CONCAT(status) AS status,GROUP_CONCAT(c) count,GROUP_CONCAT(m) month,GROUP_CONCAT(DISTINCT y) year')
            .from((subQuery) => {
            return subQuery
                .select("count(*) c,monthname(createdAt) m,year(createdAt) y,status")
                .from(file_management_entity_1.ShareFiles, "sf")
                .groupBy('year(createdAt),month(createdAt),status');
        }, "t")
            .groupBy('y')
            .addGroupBy('m')
            .getRawMany();
        const totalCustomers = await this.userRepos.findAndCount({
            where: {
                role: 'User'
            },
            withDeleted: true
        });
        const activeCustomers = await this.userRepos.count({
            where: [{
                    role: 'User',
                    isActive: true
                }],
        });
        const inactiveCustomers = await this.userRepos.count({
            where: [{
                    role: 'User',
                    isActive: false
                }],
        });
        const deactivatedCustomers = totalCustomers[0].filter((x) => x.deletedAt).length;
        if (user.role == user_enums_1.UserRole.Admin)
            return {
                totalCustomers: totalCustomers[1],
                activeCustomers,
                inactiveCustomers,
                deactivatedCustomers,
                inactiveFiles: 0,
                total_files,
                active_files,
                all_users,
                all_files
            };
        else
            return {
                totalCustomers: totalCustomers[1],
                activeCustomers,
                inactiveCustomers,
                deactivatedCustomers,
                user_completed_files,
                user_pending_files,
                user_inprogress_files,
                user_inactive_files,
                user_deleted_files,
                user_files
            };
    }
};
DashboardService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(user_repository_1.UserRepository)),
    __param(1, (0, typeorm_1.InjectRepository)(user_shared_files_repositroy_1.SharedUserRepository)),
    __param(2, (0, typeorm_1.InjectRepository)(user_shared_files_repositroy_1.SharedUserRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        user_shared_files_repositroy_1.SharedUserRepository,
        typeorm_2.Repository])
], DashboardService);
exports.DashboardService = DashboardService;
//# sourceMappingURL=dashboard.service.js.map