import { User } from './../users/entities/user.entity';
import { DashboardService } from './dashboard.service';
export declare class DashboardController {
    private readonly dashboardService;
    constructor(dashboardService: DashboardService);
    getAll(user: User): Promise<{
        totalCustomers: number;
        activeCustomers: number;
        inactiveCustomers: number;
        deactivatedCustomers: number;
        inactiveFiles: number;
        total_files: any;
        active_files: any;
        all_users: any[];
        all_files: any[];
        user_completed_files?: undefined;
        user_pending_files?: undefined;
        user_inprogress_files?: undefined;
        user_inactive_files?: undefined;
        user_deleted_files?: undefined;
        user_files?: undefined;
    } | {
        totalCustomers: number;
        activeCustomers: number;
        inactiveCustomers: number;
        deactivatedCustomers: number;
        user_completed_files: any;
        user_pending_files: any;
        user_inprogress_files: any;
        user_inactive_files: any;
        user_deleted_files: any;
        user_files: any[];
        inactiveFiles?: undefined;
        total_files?: undefined;
        active_files?: undefined;
        all_users?: undefined;
        all_files?: undefined;
    }>;
}
