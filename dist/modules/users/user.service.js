"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const mailer_1 = require("@nestjs-modules/mailer");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_enums_1 = require("../../enums/user.enums");
const typeorm_2 = require("typeorm");
const user_repository_1 = require("./repository/user.repository");
const bcrypt = require("bcrypt");
let UsersService = class UsersService {
    constructor(mailerService, userRepo) {
        this.mailerService = mailerService;
        this.userRepo = userRepo;
    }
    async create(createUserDto) {
        const result = await this.userRepo.userRegister(createUserDto, false);
        delete result.resetPasswordToken;
        delete result.password;
        delete result.salt;
        delete result.deletedAt;
        await this.mailerService
            .sendMail({
            to: result.email,
            subject: 'File Share Registration',
            template: 'src/templates/add-user.pug',
            context: {
                name: result.firstName + ' ' + (result.lastName || ''),
                email: result.email,
                password: createUserDto.password,
                role: createUserDto.isAdmin ? `Admin` : `User`,
                url: createUserDto.isAdmin ?
                    `https://dms.miraclechd.co/admin/login` :
                    `https://dms.miraclechd.co/auth/login`,
            },
        })
            .then(() => console.log('success'))
            .catch((e) => console.log(e));
        return result;
    }
    async createByCSV(createUserDto) {
        let i = 0;
        for (const iterator of createUserDto) {
            const salt = await bcrypt.genSalt(11);
            const password = createUserDto[i].password;
            console.log('salt', salt);
            console.log('i', i);
            console.log('password', password);
            createUserDto[i].salt = salt;
            createUserDto[i].isActive = createUserDto[i].isActive ? true : false;
            const passwordHash = (await this.genHash(salt, password)).toString();
            createUserDto[i].password = passwordHash;
            i++;
        }
        const result = await this.userRepo.save(createUserDto);
        return result;
    }
    async genHash(salt, password) {
        const pass = password.toString();
        const hash = await bcrypt.hash(pass, salt);
        console.log(hash);
        return hash;
    }
    async findAll(page, limit, search) {
        limit = parseInt(limit) || 10;
        page = parseInt(page) - 1 || 0;
        const [result, count] = await this.userRepo.findAndCount({
            take: limit,
            where: [{
                    firstName: (0, typeorm_2.Raw)((alias) => `LOWER(${alias}) Like '%${search}%'`),
                    role: 'User'
                }, {
                    email: (0, typeorm_2.Raw)((alias) => `LOWER(${alias}) Like '%${search}%'`),
                    role: 'User'
                }
            ],
            skip: (page >= 0 ? page : 0) * limit,
            order: {
                createdAt: "DESC",
            }
        });
        return {
            list: result,
            total: count,
        };
    }
    async getUsers() {
        const result = await this.userRepo.find({
            where: {
                role: 'User',
                isActive: 1
            },
            select: [
                'id',
                'email',
                'firstName',
                'lastName'
            ]
        });
        return {
            list: result,
        };
    }
    async getUsersEmails() {
        const result = await this.userRepo.find({
            select: [
                'email'
            ]
        });
        return {
            list: result,
        };
    }
    async findOne(id) {
        const result = await this.userRepo.findOne(id, {
            select: [
                'id',
                'createdAt',
                'email',
                'firstName',
                'isActive',
                'isNotify',
                'lastName',
                'profilePic',
                'role',
                'phoneNumber',
                'deletedAt',
                'updatedAt',
            ],
        });
        if (!result)
            throw new common_1.NotFoundException('No User Found');
        return result;
    }
    async findActive(id) {
        const result = await this.userRepo.findOne({
            where: {
                id: id,
                isActive: 1,
            }
        });
        if (!result) {
            return false;
        }
        else {
            return true;
        }
    }
    async getUserForSign(id) {
        return await this.userRepo.findOne(id);
    }
    async update(id, updateUserDto) {
        const result = await this.userRepo.findOne(id);
        if (!result)
            throw new common_1.NotFoundException('No User Found');
        const { firstName, lastName, isActive, isAdmin, profilePic, phoneNumber, isDeleted, } = updateUserDto;
        result.firstName = firstName;
        result.lastName = lastName;
        result.isActive = isActive ? true : false;
        result.deletedAt = (isDeleted === null || isDeleted === void 0 ? void 0 : isDeleted.toLowerCase()) === 'true' ? new Date() : null;
        result.profilePic = profilePic;
        result.phoneNumber = phoneNumber;
        result.role = isAdmin ? user_enums_1.UserRole.Admin : user_enums_1.UserRole.User;
        try {
            await result.save();
        }
        catch (e) {
            console.log(e);
        }
        return {
            result,
            message: 'User updated successfully',
        };
    }
    async changeNotifications(isActive, user) {
        user.isNotify = isActive;
        await user.save();
        return {
            message: `User Notifications ${isActive ? 'enabled' : 'disabled'} successfully`,
        };
    }
    async searchUser(name, email) {
        return await this.userRepo.find({
            where: {
                firstName: new RegExp(`^${name}`),
                lastName: new RegExp(`^${name}`),
            },
        });
    }
    async uploadUserImage(updateUserDto, user) {
        const { filename, is_logo } = updateUserDto;
        if (is_logo) {
        }
        else {
            const updated_user = await this.userRepo.update({ id: user.id }, {
                profilePic: filename
            });
            return updated_user;
        }
    }
};
UsersService = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, typeorm_1.InjectRepository)(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [mailer_1.MailerService,
        user_repository_1.UserRepository])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=user.service.js.map