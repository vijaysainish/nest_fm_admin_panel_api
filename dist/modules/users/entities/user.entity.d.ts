import { UserRole } from 'src/enums/user.enums';
import { ShareFiles } from 'src/modules/file-management/entities/file-management.entity';
import { BaseEntity } from 'typeorm';
export declare class User extends BaseEntity {
    id: any;
    role: UserRole;
    firstName: string;
    lastName: string;
    email: string;
    profilePic: string;
    resetPasswordToken: string;
    phoneNumber: string;
    salt: string;
    password: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    isActive: boolean;
    isNotify: boolean;
    user_id: ShareFiles[];
    validatePassword(password: string): Promise<boolean>;
}
