import { MailerService } from '@nestjs-modules/mailer';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UserRepository } from './repository/user.repository';
export declare class UsersService {
    private readonly mailerService;
    private userRepo;
    constructor(mailerService: MailerService, userRepo: UserRepository);
    create(createUserDto: CreateUserDto): Promise<any>;
    createByCSV(createUserDto: CreateUserDto[]): Promise<any>;
    genHash(salt: any, password: any): Promise<string>;
    findAll(page: any, limit: any, search: any): Promise<{
        list: User[];
        total: number;
    }>;
    getUsers(): Promise<{
        list: User[];
    }>;
    getUsersEmails(): Promise<{
        list: User[];
    }>;
    findOne(id: string): Promise<User>;
    findActive(id: any): Promise<boolean>;
    getUserForSign(id: string): Promise<User>;
    update(id: string, updateUserDto: UpdateUserDto): Promise<{
        result: User;
        message: string;
    }>;
    changeNotifications(isActive: boolean, user: User): Promise<{
        message: string;
    }>;
    searchUser(name: string, email: string): Promise<User[]>;
    uploadUserImage(updateUserDto: any, user: User): Promise<import("typeorm").UpdateResult>;
}
