import { CreateUserDto } from './../dto/create-user.dto';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { LoginDto } from 'src/modules/auth/dto/login.dto';
export declare class UserRepository extends Repository<User> {
    validateUser(loginDto: LoginDto): Promise<User>;
    userRegister(createUserDto: CreateUserDto, isAdminRegister: boolean): Promise<User>;
    genHash(salt: any, password: any): Promise<string>;
}
