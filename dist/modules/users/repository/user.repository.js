"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const common_1 = require("@nestjs/common");
const bcrypt = require("bcrypt");
const user_enums_1 = require("../../../enums/user.enums");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../entities/user.entity");
const login_dto_1 = require("../../auth/dto/login.dto");
let UserRepository = class UserRepository extends typeorm_1.Repository {
    async validateUser(loginDto) {
        const { email, password } = loginDto;
        const auth = await this.findOne({
            email,
        });
        if (auth) {
            return auth.validatePassword(password).then((res) => {
                if (res === true) {
                    return auth;
                }
                else {
                    throw new common_1.NotAcceptableException('Invalid Password');
                }
            });
        }
        else {
            throw new common_1.NotFoundException(`No User Found`);
        }
    }
    async userRegister(createUserDto, isAdminRegister) {
        const { email, password, firstName, lastName, isActive, isAdmin, phoneNumber, } = createUserDto;
        const newUser = new user_entity_1.User();
        const salt = await bcrypt.genSalt(11);
        const passwordHash = (await this.genHash(salt, password)).toString();
        newUser.email = email;
        newUser.password = passwordHash;
        newUser.lastName = lastName;
        newUser.firstName = firstName;
        newUser.phoneNumber = phoneNumber;
        newUser.salt = salt;
        newUser.isActive = isActive;
        newUser.role = isAdmin ? user_enums_1.UserRole.Admin : user_enums_1.UserRole.User;
        ;
        try {
            await newUser.save();
            return newUser;
        }
        catch (error) {
            if (error.code) {
                const err = error.code;
                if (err === 11000) {
                    throw new common_1.ConflictException('Email Already exists');
                }
                else if (error.code == 'ER_DUP_ENTRY') {
                    throw new common_1.ConflictException('Email Already exists');
                }
                else {
                    throw new common_1.InternalServerErrorException('Something went Wrong' + error.code);
                }
            }
            else {
                throw new common_1.InternalServerErrorException('Something went Wrong' + error.code);
            }
        }
    }
    async genHash(salt, password) {
        const hash = await bcrypt.hash(password, salt);
        return hash;
    }
};
UserRepository = __decorate([
    (0, typeorm_1.EntityRepository)(user_entity_1.User)
], UserRepository);
exports.UserRepository = UserRepository;
//# sourceMappingURL=user.repository.js.map