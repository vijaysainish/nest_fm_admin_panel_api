import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UsersService } from './user.service';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    create(createUserDto: CreateUserDto, user: User): Promise<any>;
    createByCSV(createUserDto: CreateUserDto[], user: User): Promise<any>;
    findAll(page: any, limit: any, search: any): Promise<{
        list: User[];
        total: number;
    }>;
    getAll(): Promise<{
        list: User[];
    }>;
    getEmailsAll(): Promise<{
        list: User[];
    }>;
    findOne(id: string, user: User): Promise<User>;
    getDetails(user: User): Promise<User>;
    updateAccount(user: User, body: UpdateUserDto): Promise<{
        result: User;
        message: string;
    }>;
    update(id: string, updateUserDto: UpdateUserDto, user: User): Promise<{
        result: User;
        message: string;
    }>;
    remove(user: User): Promise<{
        message: string;
    }>;
    activate(user: User): Promise<{
        message: string;
    }>;
    searchUser(name: string, email: string): Promise<User[]>;
    updateImage(filename: UpdateUserDto, user: User): Promise<import("typeorm").UpdateResult>;
    seeUploadedFile(image: any, res: any): any;
}
