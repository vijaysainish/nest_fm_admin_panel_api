export declare class CreateUserDto {
    firstName: string;
    lastName: string;
    password: string;
    email: string;
    profilePic: string;
    phoneNumber: string;
    isActive: boolean;
    salt: string;
    isAdmin: boolean;
}
