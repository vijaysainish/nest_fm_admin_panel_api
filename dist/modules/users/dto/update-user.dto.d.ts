export declare class UpdateUserDto {
    firstName: string;
    lastName: string;
    profilePic: string;
    phoneNumber: string;
    isActive: boolean;
    isAdmin: boolean;
    isDeleted: string;
}
