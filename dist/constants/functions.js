"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.editFileName = exports.imageFileFilter = void 0;
const path_1 = require("path");
const imageFileFilter = (req, file, callback) => {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return callback(new Error('Only image files are allowed!'), false);
    }
    callback(null, true);
};
exports.imageFileFilter = imageFileFilter;
const editFileName = (file, callback) => {
    const name = file.split('.')[0];
    const fileExtName = (0, path_1.extname)(file);
    const randomName = Array(10)
        .fill(null)
        .map(() => Math.round(Math.random() * 16).toString(16))
        .join('');
    callback(null, [
        `${name}-${randomName}${fileExtName}`,
        `${fileExtName.slice(1, fileExtName.length)}`,
    ]);
};
exports.editFileName = editFileName;
//# sourceMappingURL=functions.js.map