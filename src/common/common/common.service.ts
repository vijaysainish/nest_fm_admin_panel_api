import { Injectable } from '@nestjs/common';
import { extname } from 'path';

@Injectable()
export class CommonService {
  randomNumber(length: number) {
    const string = '0123456789';
    return this.iterateString(string, length);
  }

  randomString(length: number) {
    const string =
      '0123456789qwertyu+_)(*&^%$#@!}{[]|:;><.,/?ioplkjhgfdsazxcvbnmMNBVCXZASDFGHJKLPOIUYTREWQ';
    return this.iterateString(string, length);
  }
  randomString2(length: number) {
    const string =
      '123456789qwertyuioplkjhgfdsazxcvbnmMNBVCXZASDFGHJKLPOIUYTREWQ';
    return this.iterateString(string, length);
  }

  iterateString(string, length) {
    const stringLength = string.length;
    let generatedValue = '';
    while (generatedValue.length < length) {
      generatedValue += string.charAt(Math.floor(Math.random() * stringLength));
    }
    return generatedValue;
  }

  getNewName(file) {
    let name = file.split('.')[0];
    const fileExtName = extname(file);
    name = name.split(' ').join('-');
    const randomName = Array(10)
      .fill(null)
      .map(() => Math.round(Math.random() * 16).toString(16))
      .join('');
    return [
      `${name}-${randomName}${fileExtName}`,
      `${fileExtName.slice(1, fileExtName.length)}`,
    ];
  }
}
