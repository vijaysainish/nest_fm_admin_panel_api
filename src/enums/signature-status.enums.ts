export enum SignatureStatus {
  Pending = 'Pending',
  Completed = 'Completed',
  Declined = 'Declined',
}
