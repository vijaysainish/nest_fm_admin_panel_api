export enum FileStatus {
  Pending = 'Pending',
  Deleted = 'Deleted',
  Completed = 'Completed',
  Inactive = 'Inactive',
  InProgress = 'InProgress',
  Approved = 'Approved',
}
