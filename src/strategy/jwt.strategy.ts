import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtPayload } from 'src/interfaces/jwt-payload.interface';
import { UserRepository } from 'src/modules/users/repository/user.repository';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(UserRepository) private userRepos: UserRepository,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey:
        '#E$8>%eAXpEQsPh[5spbXUhCkNs;C$]KSAjfM0Q>gfV<inK(1zjR|16:)4g.M@@1SlI{LSF8RKWXM%/OWn&|9;<,L[GO,PPyyAWdG<J)c6a&2mHZdu62rqq+gQ|DkF_Z/*tTbB!aIH1c{o,$.wniFz',
    });
  }
  async validate(payload: JwtPayload) {
    const { id } = payload;
    const user = await this.userRepos.findOne(id);
    if (!user) return new UnauthorizedException();
    return user;
  }
}
