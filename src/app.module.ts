import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './modules/auth/auth.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { UploadModule } from './modules/upload/upload.module';
import { UsersModule } from './modules/users/users.module';
import * as nodemailer from 'nodemailer';
import { FileManagementModule } from './modules/file-management/file-management.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { SignatureModule } from './modules/signature/signature.module';
import { NotificationsModule } from './modules/notifications/notifications.module';
// import { MulterModule } from '@nestjs/platform-express';

import { AdminController } from './modules/admin/admin.controller';
import { AdminModule } from './modules/admin/admin.module';
@Module({
  imports: [
    UsersModule,
    AuthModule,
    ConfigModule.forRoot({ isGlobal: true }),

    MailerModule.forRoot({
      transport: {
        // host: 'smtp.gmail.com',
        host: 'smtp.forrce.com',
        port: 465,
        // ignoreTLS: true,
        secure: true,
        tls: { rejectUnauthorized: false },
        auth: {
          user: process.env.MAILDEV_INCOMING_USER,
          pass: process.env.MAILDEV_INCOMING_PASS,
        },
      },
      defaults: {
        from: '"No Reply" <no-reply@localhost>',
      },
      preview: false,
      template: {
        dir: process.cwd() + '/src/templates/',
        adapter: new PugAdapter(), // or new PugAdapter() or new EjsAdapter()
        options: {
          strict: true,
        },
      },
    }),
    //  MailerModule.forRoot({
    //   transport: process.env.TRANSPORT,
    //   port: 465,
    //   secure: true,
    //   defaults: {
    //     from:'File Share | Link <jerry@jerryondemand.com>'
        
    //   },
    //   template: {
    //     adapter: new PugAdapter(), // or new PugAdapter()
    //     options: {
    //       strict: true,
    //     },
    //   },
    // }),


    // MailerModule.forRoot({
    //   // transport: nodemailer.createTransport('smtps://vijaysainish@gmail.com:9996618069@smtp.gmail.com'),
    //   // transport:  process.env.TRANSPORT,
    //   transport: nodemailer.createTransport({
    //     host: 'smtp.gmail.com',
    //     port: 587,
    //     secure: false,
    //     auth: {
    //       user: 'vijaysainish@gmail.com',
    //       pass: '9996618069',
    //     },
    //   }),
    //   defaults: {
    //     from: '"DMS" <<email>>',
    //   },
    //   template: {
    //     dir: __dirname + '/templates',
    //     adapter: new PugAdapter({
    //       inlineCssEnabled: true,
    //       inlineCssOptions: { url: ' ' },
    //     }),
    //     options: {
    //       strict: true,
    //     },
    //   },
    // }),
    // TypeOrmModule.forRoot({
    //   type: 'mongodb',
    //   database: 'fileShare',
    //   url: '<Mongo DB URL>',
    //   useNewUrlParser: true,
    //   useUnifiedTopology: true,
    //   synchronize: true,
    //   logging: true,
    //   logger: 'debug',
    //   entities: [join(__dirname, './**/**.entity{.ts,.js}')],
    // }),
    TypeOrmModule.forRoot(),
    DashboardModule,
    UploadModule,
    FileManagementModule,
    SignatureModule,
    NotificationsModule,
    AdminModule,
  
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
