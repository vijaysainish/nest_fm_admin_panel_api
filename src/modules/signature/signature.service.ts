import { FileManagementService } from './../file-management/file-management.service';
import { UsersService } from './../users/user.service';
import { SignatureRepository } from './repository/signature.repository';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateSignatureDto } from './dto/create-signature.dto';
import { UpdateSignatureDto } from './dto/update-signature.dto';

@Injectable()
export class SignatureService {
  async getFileSignatures(id: string) {
    console.log(id);
    return await this.signatureRepository.find({ where: { file: id } });
  }
  constructor(
    @InjectRepository(SignatureRepository)
    private signatureRepository: SignatureRepository,
    private usersService: UsersService,
    private fileService: FileManagementService,
  ) {}

  async create(createSignatureDto: CreateSignatureDto) {
    const { userId, fileId, userEmail } = createSignatureDto;
    let user;
    if (userId) {
      user = await this.usersService.getUserForSign(userId);
    }
    const file = await this.fileService.getfile(fileId);
    if (!user && !userEmail)
      throw new BadRequestException('Please enter user email');
    return this.signatureRepository.newSigns(createSignatureDto, user, file);
  }

  findAll() {
    return `This action returns all signature`;
  }

  findOne(id: number) {
    return `This action returns a #${id} signature`;
  }

  update(id: number, updateSignatureDto: UpdateSignatureDto) {
    return `This action updates a #${id} signature`;
  }

  remove(id: number) {
    return `This action removes a #${id} signature`;
  }
}
