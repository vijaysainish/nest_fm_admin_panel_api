import { FileManagementService } from './../file-management/file-management.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/modules/users/repository/user.repository';
import { FileManagementRepository } from '../file-management/repository/file-management.repository';
import { FileManagementModule } from './../file-management/file-management.module';
import { UsersService } from './../users/user.service';
import { SignatureRepository } from './repository/signature.repository';
import { SignatureController } from './signature.controller';
import { SignatureService } from './signature.service';
import { SharedUserRepository } from '../file-management/repository/user-shared-files.repositroy';
import {CommentRepository  } from '../file-management/repository/file-comment.repositroy';
import { NotificationRepository } from '../notifications/repository/notifications.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      SignatureRepository,
      UserRepository,
      FileManagementRepository,
      SharedUserRepository,
      CommentRepository,
      NotificationRepository
    ]),
    FileManagementModule,
  ],
  controllers: [SignatureController],
  providers: [SignatureService, UsersService, FileManagementService],
})
export class SignatureModule {}
