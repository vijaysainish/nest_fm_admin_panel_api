import { IsOptional, IsString } from 'class-validator';
export class CreateSignatureDto {
  @IsString()
  @IsOptional()
  userId: string;

  @IsString()
  fileId: string;

  @IsString()
  @IsOptional()
  userName: string;

  @IsString()
  @IsOptional()
  userEmail: string;

  @IsString()
  @IsOptional()
  signatureFileName: string;
}
