import { BaseEntity, Column, Entity, ObjectIdColumn,PrimaryGeneratedColumn } from 'typeorm';
import { SignatureStatus } from './../../../enums/signature-status.enums';

@Entity()
export class Signature extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: any;

  @Column()
  file: string;

  @Column()
  userToken: string;

  @Column()
  index: number;

  @Column()
  user: string;

  @Column()
  userName: string;

  @Column()
  userEmail: string;

  @Column({
    type: 'enum',
    enum: SignatureStatus,
    default: SignatureStatus.Pending,
  })
  status: SignatureStatus;

  @Column()
  comment: string;

  @Column()
  signFile: string;
}
