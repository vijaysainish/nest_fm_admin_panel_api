import { InternalServerErrorException } from '@nestjs/common';
import { ShareFiles } from 'src/modules/file-management/entities/file-management.entity';
import { User } from 'src/modules/users/entities/user.entity';
import { EntityRepository, Repository } from 'typeorm';
import { CreateSignatureDto } from './../dto/create-signature.dto';
import { Signature } from './../entities/signature.entity';

@EntityRepository(Signature)
export class SignatureRepository extends Repository<Signature> {
  async newSigns(createSignature: CreateSignatureDto, user: User, file: ShareFiles) {
    const { userId, userEmail, userName, signatureFileName } = createSignature;
    const newSign = new Signature();
    if (userId) {
      newSign.user = user.id.toString();
    } else {
      newSign.userEmail = userEmail;
      newSign.userName = userName;
    }
    newSign.file = file.id.toString();
    newSign.signFile = signatureFileName;
    try {
      await newSign.save();
      return newSign;
    } catch (error) {
      throw new InternalServerErrorException('Something Went wrong');
    }
  }
}
