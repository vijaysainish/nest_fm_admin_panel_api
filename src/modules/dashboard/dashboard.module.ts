import { AuthModule } from './../auth/auth.module';
import { Module } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { DashboardController } from './dashboard.controller';
import { UserRepository } from '../users/repository/user.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './../users/user.service';
import { SharedUserRepository } from '../file-management/repository/user-shared-files.repositroy';

@Module({
  imports: [AuthModule, TypeOrmModule.forFeature([UserRepository,SharedUserRepository])],
  controllers: [DashboardController],
  providers: [DashboardService,UsersService],
})
export class DashboardModule {}
