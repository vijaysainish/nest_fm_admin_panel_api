import { Controller, Get, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/auth.guard';
import { RoleGuard } from '../auth/role.guard';
import { GetUser } from './../../decorators/getUser.decorator';
import { User } from './../users/entities/user.entity';
import { DashboardService } from './dashboard.service';

@Controller('dashboard')
export class DashboardController {
  constructor(private readonly dashboardService: DashboardService) {}

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Get()
  getAll(@GetUser() user: User) {
    return this.dashboardService.findAll(user);
  }
}
