import { User } from './../users/entities/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '../users/repository/user.repository';
import { UserRole } from 'src/enums/user.enums';
import { FileManagementRepository } from '../file-management/repository/file-management.repository';
import { SharedUserRepository } from '../file-management/repository/user-shared-files.repositroy';
import { Brackets, createQueryBuilder, getConnection, getRepository, Repository } from 'typeorm';
import { SharedUser } from '../file-management/entities/user-shared-files.entity';
import { distinct } from 'rxjs';
import { ShareFiles } from '../file-management/entities/file-management.entity';
import { FileStatus } from 'src/enums/file-status.enums';

@Injectable()
export class DashboardService {
  constructor(
    @InjectRepository(UserRepository) private userRepos: UserRepository,
    @InjectRepository(SharedUserRepository) private sharedUsers: SharedUserRepository,
    @InjectRepository(SharedUserRepository)
    private readonly sharedUserRepository: Repository<SharedUserRepository>,
  ) {}
  async findAll(user: User) {
    const total_files = await getConnection()
    .createQueryBuilder()
    .select('COUNT(DISTINCT shareduser.file_id)','total_files_count')
    .from(SharedUser,'shareduser')
    // .select("shared_user")
    // .addSelect("COUNT(shareduser.file_id)", "files_total_count" )
    // .where('shareduser.initiator_id'+user.id)
    // .where("shareduser.initiator_id = :id", { id: user.id })
    // .groupBy("shareduser.initiator_id")
    // .distinct(true)
    .getRawOne();

    const active_files = await getConnection()
    .createQueryBuilder()
    .select('COUNT(DISTINCT shareFiles.id)','active_files_count')
    .from(SharedUser,'shareduser')
    .innerJoin(ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
    // .where("shareduser.initiator_id = :id", { id: user.id })
    .andWhere("shareFiles.status = :status", { status: FileStatus.Completed })
    .getRawOne();

    const user_completed_files = await getConnection()
    .createQueryBuilder()
    .select('COUNT(DISTINCT shareFiles.id)','count')
    .from(SharedUser,'shareduser')
    .innerJoin(ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
    .where("shareFiles.status = :status", { status: FileStatus.Completed})
    .andWhere(
      new Brackets((qb) => {
        qb.where("shareduser.initiator_id = :id", { id: user.id })
        .orWhere("shareduser.user_id = :uid", { uid: user.id })
      })
    )
    .getRawOne();

    const user_pending_files = await getConnection()
    .createQueryBuilder()
    .select('COUNT(DISTINCT shareFiles.id)','count')
    .from(SharedUser,'shareduser')
    .innerJoin(ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
    .where("shareFiles.status = :status", { status: FileStatus.Pending})
    .andWhere(
      new Brackets((qb) => {
        qb.where("shareduser.initiator_id = :id", { id: user.id })
        .orWhere("shareduser.user_id = :uid", { uid: user.id })
      })
    )
    .getRawOne();
    const user_inprogress_files = await getConnection()
    .createQueryBuilder()
    .select('COUNT(DISTINCT shareFiles.id)','count')
    .from(SharedUser,'shareduser')
    .innerJoin(ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
    .where("shareFiles.status = :status", { status: FileStatus.InProgress})
    .andWhere(
      new Brackets((qb) => {
        qb.where("shareduser.initiator_id = :id", { id: user.id })
        .orWhere("shareduser.user_id = :uid", { uid: user.id })
      })
    )
    .getRawOne();
    const user_inactive_files = await getConnection()
    .createQueryBuilder()
    .select('COUNT(DISTINCT shareFiles.id)','count')
    .from(SharedUser,'shareduser')
    .innerJoin(ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
    .where("shareFiles.status = :status", { status: FileStatus.Inactive})
    .andWhere(
      new Brackets((qb) => {
        qb.where("shareduser.initiator_id = :id", { id: user.id })
        .orWhere("shareduser.user_id = :uid", { uid: user.id })
      })
    )
    .getRawOne();
    const user_deleted_files = await getConnection()
    .createQueryBuilder()
    .withDeleted()
    .select('COUNT(DISTINCT shareFiles.id)','count')
    .from(SharedUser,'shareduser')
    .innerJoin(ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
    .where("shareFiles.status = :status", { status: FileStatus.Deleted})
  
    .andWhere(
      new Brackets((qb) => {
        qb.where("shareduser.initiator_id = :id", { id: user.id })
        .orWhere("shareduser.user_id = :uid", { uid: user.id })
      })
    )
    .getRawOne();
    const user_files = await getConnection()
    .createQueryBuilder()
    .select('shareFiles.*')
    // .where("is_sharing=:val",{val:0})
    .from(SharedUser,'shareduser')
    .innerJoin(ShareFiles, 'shareFiles', 'shareFiles.id = shareduser.file_id')
    .andWhere(
      new Brackets((qb) => {
        qb.where("shareduser.initiator_id = :id", { id: user.id })
        .orWhere("shareduser.user_id = :uid", { uid: user.id })
      })
    )
    .groupBy('file_id')
    .limit(10)
    .getRawMany();

   

    const all_users  = await getConnection()
    .createQueryBuilder()
    .select('GROUP_CONCAT(isActive) AS status,GROUP_CONCAT(c) count,GROUP_CONCAT(m) month,GROUP_CONCAT(DISTINCT y) year')
    .from((subQuery) => {
      return subQuery
          .select("count(*) c,monthname(createdAt) m,year(createdAt) y,isActive")
          .from(User, "user")
          .groupBy('year(createdAt),month(createdAt),isActive')
    }, "users")
    .groupBy('y')
    .addGroupBy('m')
    .getRawMany();

  
    

    const all_files   = await getConnection()
    .createQueryBuilder()
    .select('GROUP_CONCAT(status) AS status,GROUP_CONCAT(c) count,GROUP_CONCAT(m) month,GROUP_CONCAT(DISTINCT y) year')
    .from((subQuery) => {
      return subQuery
          .select("count(*) c,monthname(createdAt) m,year(createdAt) y,status")
          .from(ShareFiles, "sf")
          .groupBy('year(createdAt),month(createdAt),status')
    }, "t")
    .groupBy('y')
    .addGroupBy('m')
    .getRawMany();

    // return all_users;
    
    const totalCustomers = await this.userRepos.findAndCount({
      where: {
       role:'User' 
      },
      withDeleted: true 
    });
    const activeCustomers = await this.userRepos.count( {
      where: [{
        role:'User',
        isActive:true
     }],
    });
    const inactiveCustomers = await this.userRepos.count( {
      where: [{
        role:'User',
        isActive:false
     }],
    });
    // const inactiveCustomers = totalCustomers[1] - activeCustomers;
    const deactivatedCustomers = totalCustomers[0].filter(
      (x) => x.deletedAt,
    ).length;
    if (user.role == UserRole.Admin)
      return {
        totalCustomers: totalCustomers[1],
        activeCustomers,
        inactiveCustomers,
        deactivatedCustomers,
        inactiveFiles: 0,
        total_files,
        active_files,
        all_users,
        all_files
      };
    else
      return {
        totalCustomers: totalCustomers[1],
        activeCustomers,
        inactiveCustomers,
        deactivatedCustomers,
        user_completed_files,
        user_pending_files,
        user_inprogress_files,
        user_inactive_files,
        user_deleted_files,
        user_files
      };
  }
}
