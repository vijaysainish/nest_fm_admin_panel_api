import { Injectable, StreamableFile, NotFoundException } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { CommonService } from './../../common/common/common.service';
const s3 = new AWS.S3({
  accessKeyId: 'AKIAYHUDVK2HX7TDAIQ3',
  secretAccessKey: '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',
});

@Injectable()
export class UploadService {
  constructor(private cs: CommonService) {}
  async uploadImage(img) {
    const [name, extension] = this.cs.getNewName(img.originalname);
    // return img;
    const params = {
      Bucket: 'force-dev/images',
      Key: name,
      Body: img.buffer,
    };
    return await s3
      .upload(params)
      .promise()
      .then(
        (data) => data,
        (err) => err,
      );
  }

  async getFile(fileName, folder) {
    const params = {
      Bucket: `force-dev/${folder}`,
      Key: fileName,
    };
    return await s3
      .headObject(params)
      .promise()
      .then(() => {
        let stream = s3.getObject(params).createReadStream();
        return new StreamableFile(stream);
      })
      .catch((error) => {
        throw new Error(error);
      });
  }

  async uploadPDF(pdf) {
    const [name, extension] = this.cs.getNewName(pdf.originalname);
    const params = {
      Bucket: 'force-dev/pdfs',
      Key: name,
      Body: pdf.buffer,
    };
    return await s3
      .upload(params)
      .promise()
      .then(
        (data) => data,
        (err) => err,
      );
  }
}
