import { CommonService } from './../../common/common/common.service';
import { Module } from '@nestjs/common';
import { UploadService } from './upload.service';
import { UploadController } from './upload.controller';

@Module({
  controllers: [UploadController],
  providers: [UploadService, CommonService],
})
export class UploadModule {}
