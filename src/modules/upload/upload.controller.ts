import {
  Controller,
  Get,
  Param,
  Post,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { User } from './../users/entities/user.entity';
import { diskStorage } from 'multer';
import { GetUser } from 'src/decorators/getUser.decorator';
import { JwtAuthGuard } from '../auth/auth.guard';
import { RoleGuard } from '../auth/role.guard';
import { UploadService } from './upload.service';

@Controller('file')
export class UploadController {
  constructor(private readonly uploadService: UploadService) {}

  // @Post('upload/image')
  // @UseInterceptors(FileInterceptor('file'))
  // image(@UploadedFile() image: any) {

  //   // return this.uploadService.uploadImage(image);
  // }

  @Post('upload/image')
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: './uploads/images',
      // filename: editFileName,
    }),
    // fileFilter: imageFileFilter,
  }))
  image(@UploadedFile() file: Express.Multer.File,@GetUser() user: User) {
    console.log(file);
   return file;
  }

  @Get('/image/:img')
  findImg(@Param('img') img: string) {
    return this.uploadService.getFile(img, 'images');
  }

  @Get('/pdf/:pdf')
  findPpdf(@Param('pdf') pdf: string) {
    return this.uploadService.getFile(pdf, 'pdfs');
  }

  @Post('upload/pdf')
  @UseInterceptors(FileInterceptor('pdf'))
  pdf(@UploadedFile() pdf: any) {
    return this.uploadService.uploadPDF(pdf);
  }
}
