import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { SignatureService } from './../signature/signature.service';
import { CreateFileManagementDto } from './dto/create-file-management.dto';
import { ShareFileDto } from './dto/share-file.dto';
import { UpdateFileManagementDto } from './dto/update-file-management.dto';
import { FileManagementRepository } from './repository/file-management.repository';

import { SharedUserRepository } from './repository/user-shared-files.repositroy';
import { CommentRepository } from './repository/file-comment.repositroy';
import { NotificationRepository } from '../notifications/repository/notifications.repository';
import { SharedUser } from './entities/user-shared-files.entity';
import { FileStatus } from 'src/enums/file-status.enums';
import { UpdateFileRequestDto } from './dto/update-file-request.dto copy';
import { SearchFileRequestDto } from './dto/search.dto';
import { In, Like, Raw } from 'typeorm';
import { MailerService } from '@nestjs-modules/mailer';
import { UpdateOtherUserFiletDto } from './dto/update-other-user-file.dto';
import { saveEditFileDto } from './dto/save-edit-file.dto';
import { UserRole } from 'src/enums/user.enums';

@Injectable()
export class FileManagementService {
  constructor(
    @InjectRepository(FileManagementRepository)
    private fileRepo: FileManagementRepository,
    @InjectRepository(SharedUserRepository)
    private sharedUserRepo: SharedUserRepository,
    @InjectRepository(CommentRepository)
    private commentRepo: CommentRepository,
    @InjectRepository(NotificationRepository)
    private notifyRepo: NotificationRepository,
    private readonly mailerService: MailerService
  ) { }

  async create(createFileManagementDto: CreateFileManagementDto, user: User) {

    const new_file = await this.fileRepo.newFile(createFileManagementDto, user.id);
    const shared_user = createFileManagementDto.SharedUser;
    const file_data = createFileManagementDto.FileData;
    let shared_data_arr = [];
    let notify_data_arr = [];
    let comment_data_arr = [{
      comment: file_data.comment,
      file_id: new_file.id,
      user_id: user.id
    }];
    let send_to: any;
    await shared_user.user_ids.forEach(async(id, i) => {
      // if (i == 0) {
      //   send_to = id;
      // }
      const data = {
        file_id: new_file.id,
        user_id: typeof id == 'number' ? id : null,
        other_user_name: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[1],
        other_user_email: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[0],
        initiator_id: user.id,
        is_shared: 1,
        is_locked: i == 0 ? 0 : 1
      }
      const notify_data = {
        send_to: typeof id == 'number' ? id : null,
        send_by: user.id,
        status: 1,
        is_unread: 1,
        request_sent: 1,
        file_id: new_file.id
      };
      shared_data_arr.push(data);
      notify_data_arr.push(notify_data);
    
    if (typeof id != 'number') {
     
      await this.mailerService
          .sendMail({
            to: id.split(/(?<=^\S+)\s/)[0],
            // from: 'support@playgroundraters.com',
            subject: 'File Shared for Sign',
            template: `src/templates/other-signer-user.pug`,
            context: {
              name: id.split(/(?<=^\S+)\s/)[1],
              email: id.split(/(?<=^\S+)\s/)[0],
              start_date:createFileManagementDto.FileData.startDate,
              end_date:createFileManagementDto.FileData.endDate,
              // role: createUserDto.isAdmin ? `Admin` : `User`,
              url: `https://dms.miraclechd.co/other-user?id=${new_file.id}&name=${id.split(/(?<=^\S+)\s/)[1]}&email=${id.split(/(?<=^\S+)\s/)[0]}`
              // url: `https://dms.miraclechd.co/other-user?id=${new_file.id}&name=${id.split(' ')[1]}&email=${id.split(' ')[0]}`
              // url: `http://localhost:4200/other-user?id=${new_file.id}&name=${id.split(/(?<=^\S+)\s/)[1]}&email=${id.split(/(?<=^\S+)\s/)[0]}`
            },
          })
          .then(() => console.log('success'))
          .catch((e) => console.log(e));
      }
    });


    await this.sharedUserRepo.save(shared_data_arr);
    if (file_data.comment) {
      await this.commentRepo.save(comment_data_arr);
    }
    await this.notifyRepo.save(notify_data_arr)
    // return this.fileRepo.query(`SELECT fileName from share_files WHERE id = ${new_file.id}`)
    return new_file;
  }


  async saveFile(createFileManagementDto: CreateFileManagementDto, user: User) {

    const new_file = await this.fileRepo.newFile(createFileManagementDto, user.id,1);
    const shared_user = createFileManagementDto.SharedUser;
    const file_data = createFileManagementDto.FileData;
    let shared_data_arr = [];

    let comment_data_arr = [{
      comment: file_data.comment,
      file_id: new_file.id,
      user_id: user.id
    }];
    let send_to: any;
    await shared_user.user_ids.forEach(async(id, i) => {
      // if (i == 0) {
      //   send_to = id;
      // }
      const data = {
        file_id: new_file.id,
        user_id: typeof id == 'number' ? id : null,
        other_user_name: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[1],
        other_user_email: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[0],
        initiator_id: user.id,
        is_shared: 1,
        is_locked: i == 0 ? 0 : 1
      }
      shared_data_arr.push(data);
    });


    await this.sharedUserRepo.save(shared_data_arr);
    if (file_data.comment) {
      await this.commentRepo.save(comment_data_arr);
    }
    return new_file;
  }

  async updateFile(updateFileManagementDto: UpdateFileManagementDto, user: User) {

    // const update_file =  await this.fileRepo.updateFile(updateFileManagementDto, user.id);
    const result = await this.fileRepo.findOne({
      id: updateFileManagementDto.file_id
    });
    const shared_user = await this.sharedUserRepo.findOne({
      file_id: updateFileManagementDto.file_id,
      user_id: user.id
    });

    if (result && shared_user) {
      // replace new file name with old file name
      result.fileName = updateFileManagementDto.filename;
      await result.save();
      // locked file after signed
      shared_user.is_locked = 1;
      shared_user.sign_status = FileStatus.Completed;
      await shared_user.save();

      // get next shard user
      const next_user = await this.sharedUserRepo.findOne({
        file_id: updateFileManagementDto.file_id,
        sign_status: FileStatus.Pending
      });
      if (next_user) {
        // shared with next user
        next_user.is_locked = 0;
        await next_user.save();

        // for send nofication to owner
        let notify_data_arr = [{
          send_to: next_user.initiator_id,
          send_by: user.id,
          status: 2,
          is_unread: 1,
          file_id: updateFileManagementDto.file_id
        },
        {
          send_by: next_user.initiator_id,
          send_to: next_user.user_id,
          status: 3,
          is_unread: 1,
          file_id: updateFileManagementDto.file_id
        }
        ];
        await this.notifyRepo.save(notify_data_arr)
      } else {
        let notify_data_arr = [{
          send_to: shared_user.initiator_id,
          send_by: user.id,
          status: 2,
          is_unread: 1,
          file_id: updateFileManagementDto.file_id
        }];
        result.status = FileStatus.Completed;
        await result.save();
        await this.notifyRepo.save(notify_data_arr)
      }

    } else {
      throw new NotFoundException('No User Found');
    }
    return {
      message: 'File has been updated successfully',
    };
  }



  async updateOtherUserFile(updateOtherUserFiletDto: UpdateOtherUserFiletDto) {

    // const update_file =  await this.fileRepo.updateFile(updateFileManagementDto, user.id);
    const result = await this.fileRepo.findOne({
      id: updateOtherUserFiletDto.file_id
    });
    const shared_user = await this.sharedUserRepo.findOne({
      file_id: updateOtherUserFiletDto.file_id,
      other_user_email: updateOtherUserFiletDto.email,
      other_user_name: updateOtherUserFiletDto.name
    });

    if (result && shared_user) {
      // replace new file name with old file name
      result.fileName = updateOtherUserFiletDto.filename;
      await result.save();
      // locked file after signed
      shared_user.is_locked = 1;
      shared_user.sign_status = FileStatus.Completed;
      await shared_user.save();

      // get next shard user
      const next_user = await this.sharedUserRepo.findOne({
        file_id: updateOtherUserFiletDto.file_id,
        sign_status: FileStatus.Pending
      });
      if (next_user) {
        // shared with next user
        next_user.is_locked = 0;
        await next_user.save();

        // for send nofication to owner
        let notify_data_arr = [{
          send_to: next_user.initiator_id,
          send_by: null,
          status: 2,
          is_unread: 1,
          file_id: updateOtherUserFiletDto.file_id,
          message: `${updateOtherUserFiletDto.name}, Other user updated signed on file`
        },
        {
          send_by: next_user.initiator_id,
          send_to: next_user.user_id,
          status: 3,
          is_unread: 1,
          file_id: updateOtherUserFiletDto.file_id
        }
        ];
        await this.notifyRepo.save(notify_data_arr)
      } else {
        let notify_data_arr = [{
          send_to: shared_user.initiator_id,
          send_by: null,
          status: 2,
          is_unread: 1,
          file_id: updateOtherUserFiletDto.file_id,
          message: `${updateOtherUserFiletDto.name}, Other user updated signed on file`
        }];
        result.status = FileStatus.Completed;
        await result.save();
        await this.notifyRepo.save(notify_data_arr)
      }

    } else {
      throw new NotFoundException('No User Found');
    }
    return {
      message: 'File has been updated successfully',
    };
  }


  async editReshareFile(updateFileManagementDto: UpdateFileManagementDto, user: User) {

    // const update_file =  await this.fileRepo.updateFile(updateFileManagementDto, user.id);
    const result = await this.fileRepo.findOne({
      id: updateFileManagementDto.file_id
    });
    // fetch rejected request users
    const shared_users = await this.sharedUserRepo.find({
      file_id: updateFileManagementDto.file_id,
      is_accept: 2
    });

    if (result && shared_users.length) {
      // replace new file name with old file name
      result.fileName = updateFileManagementDto.filename;
      result.is_editable = 0;
      await result.save();
      // locked file after signed
      let notify_data_arr = [];
      shared_users.forEach(element => {
        let data = {
          send_to: element.user_id,
          send_by: user.id,
          status: 7,
          is_unread: 1,
          request_sent: 1,
          file_id: updateFileManagementDto.file_id
        };
        notify_data_arr.push(data);
      });

      await this.notifyRepo.save(notify_data_arr)
    } else {
      throw new NotFoundException('No User Found');
    }
    return {
      message: 'File has been updated successfully',
    };
  }

  async editSharingFile(saveEditFile: saveEditFileDto, user: User) {

    // return saveEditFile;

    // const update_file =  await this.fileRepo.updateFile(updateFileManagementDto, user.id);
    const result = await this.fileRepo.findOne({
      id: saveEditFile.FileData.file_id
    });
   

    if (result) {
      // replace new file name with old file name
      result.fileName = saveEditFile.FileData.filename;
      if(!result.is_sharing) {
        result.is_editable = 0;
      }
      // result.is_editable = 1;
      await result.save();
      const new_shared_user_arr = [];
      const rm_shared_user_arr = [];
      const rm_shared_other_user_arr = [];
      await saveEditFile.SharedUser.new_user_ids.forEach(async(id, i) => {
        const data = {
          file_id: saveEditFile.FileData.file_id,
          user_id: typeof id == 'number' ? id : null,
          other_user_name: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[1],
          other_user_email: typeof id == 'number' ? null : id.split(/(?<=^\S+)\s/)[0],
          initiator_id: user.id,
          is_shared: 1,
          is_locked:  1
        }
        new_shared_user_arr.push(data);
      });
      await saveEditFile.SharedUser.rm_user_ids.forEach(async(id, i) => {
      const file_id  = saveEditFile.FileData.file_id;
        if(typeof id == 'number') {
          // const udata = {
          //   file_id: saveEditFile.FileData.file_id,
          //   user_id:  id 
          // }
          // rm_shared_user_arr.push(udata);
          await this.sharedUserRepo.delete({file_id,user_id:id});
        }
        if(typeof id != 'number') {
          // const otherdata = {
          //   file_id: saveEditFile.FileData.file_id,
          //   other_user_name:  id.split(/(?<=^\S+)\s/)[1],
          //   other_user_email:  id.split(/(?<=^\S+)\s/)[0],
          // }
         const other_user_name =  id.split(/(?<=^\S+)\s/)[1];
         const other_user_email =  id.split(/(?<=^\S+)\s/)[0];
          await this.sharedUserRepo.delete({file_id,other_user_name,other_user_email});
          // rm_shared_other_user_arr.push(otherdata);
        }
        
       
      });
  
      // return rm_shared_user_arr;
      await this.sharedUserRepo.save(new_shared_user_arr);
      const shared_users = await this.sharedUserRepo.find({
        relations:['user'],
        where: {
          file_id: saveEditFile.FileData.file_id,
          is_accept:2
        }
      });

     // notify users by which file was rejected
        const notify_data_arr = [];
      await shared_users.forEach(async(shared_user_data:any, i) => {
      
        const notify_data = {
          send_to: shared_user_data.user_id,
          send_by: user.id,
          status: 7,
          is_unread: 1,
          request_sent: 1,
          file_id:  saveEditFile.FileData.file_id
        };
        notify_data_arr.push(notify_data);
    
      });
      await this.notifyRepo.save(notify_data_arr);
      await shared_users.forEach(async(shared_user_data:any, i) => {
        await this.sharedUserRepo.update(
          { file_id: saveEditFile.FileData.file_id }, {
          is_accept: 0
        });
      });

     
     
    } else {
      throw new NotFoundException('No File Found');
    }
    return {
      message: 'File has been updated successfully',
    };
  }
  async shareFileFromList(shareFileDto: ShareFileDto, user: User) {

    await this.fileRepo.update({
      id:shareFileDto.file_id,
     
    },{
      startDate:shareFileDto.startDate,
      endDate:shareFileDto.endDate,
      is_sharing:0,
      status:FileStatus.InProgress,
      is_editable:0
    })

   
    await this.commentRepo.upsert({
      id:shareFileDto.comment_id,
      file_id:shareFileDto.file_id,
      user_id:user.id,
      comment:shareFileDto.comment
    },  { conflictPaths: ['id'] });

    // // OR

    // await this.commentRepo.upsert({
    //   id:shareFileDto.comment_id,
    //   file_id:shareFileDto.file_id,
    //   user_id:user.id,
    //   comment:shareFileDto.comment
    // },  ['id']); // it may by be ['id','file_id'] for multipal conflicts, or  { conflictPaths: ['id'] }

   const shared_users = await this.sharedUserRepo.find({
      where: {
        file_id: shareFileDto.file_id,
      }
    });
    // return shared_users;
    const notify_data_arr = [];
    await shared_users.forEach(async(shared_user_data:any, i) => {
     
      const notify_data = {
        send_to: shared_user_data.user_id,
        send_by: user.id,
        status: 1,
        is_unread: 1,
        request_sent: 1,
        file_id: shareFileDto.file_id
      };
      notify_data_arr.push(notify_data);
    
    if (!shared_user_data.id) {
     
      await this.mailerService
          .sendMail({
            to: shared_user_data.other_user_email,
            // from: 'support@playgroundraters.com',
            subject: 'File Shared for Sign',
            template: `src/templates/other-signer-user.pug`,
            context: {
              name:shared_user_data.other_user_name,
              email: shared_user_data.other_user_email,
              start_date:shareFileDto.startDate,
              end_date:shareFileDto.endDate,
              // role: createUserDto.isAdmin ? `Admin` : `User`,
              url: `https://dms.miraclechd.co/other-user?id=${shared_user_data.id}&name=${shared_user_data.other_user_name}&email=${shared_user_data.other_user_email}`
              // url: `https://dms.miraclechd.co/other-user?id=${new_file.id}&name=${id.split(' ')[1]}&email=${id.split(' ')[0]}`
              // url: `http://localhost:4200/other-user?id=${new_file.id}&name=${id.split(/(?<=^\S+)\s/)[1]}&email=${id.split(/(?<=^\S+)\s/)[0]}`
            },
          })
          .then(() => console.log('success'))
          .catch((e) => console.log(e));
      }
    });
    // return notify_data_arr;
   
    await this.notifyRepo.save(notify_data_arr)
     return shared_users;
  }

  findAll() {
    return this.fileRepo.find();
  }
  async getFiles(user: User,page, limit) {
    limit = parseInt(limit) || 10;
    page = parseInt(page) - 1 || 0;
    if (user.role != 'User') {
      const [result, count] =  await this.fileRepo.findAndCount({
        // where: {
        //   is_sharing: 0,
        // },
        relations: ["comment"],
        take: limit,
        skip: (page >= 0 ? page : 0) * limit,
        order: {
          createdAt: "DESC",
      },
      });
      return {
        list: result,
        total: count,
      };
    } else {
      const [result, count] = await this.fileRepo.findAndCount({
        take: limit,

        where: {
          initiator_id: user.id,
        },
        relations: ["comment"],
        skip: (page >= 0 ? page : 0) * limit,
        order: {
          createdAt: "DESC",
      },
      });
      return {
        list: result,
        total: count,
      };
    }
   
    // return this.sharedUserRepo.find({ relations: ["user",'owner','file'] });
  }
 async getSharingFiles(user: User,page, limit) {
    limit = parseInt(limit) || 10;
    page = parseInt(page) - 1 || 0;
    if (user.role == 'Admin') {
      const [result, count] = await this.fileRepo.findAndCount({
        take: limit,
        where: {
          // initiator_id: user.id,
          is_sharing:1
        },
        relations: ["comment"],
        skip: (page >= 0 ? page : 0) * limit,
        order: {
          createdAt: "DESC",
      },
      });
      return {
        list: result,
        total: count,
      };
    }
  }
  async getSharedWithMeFiles(user: User,page, limit) {
    limit = parseInt(limit) || 10;
    page = parseInt(page) - 1 || 0;
    if (user.role != 'User') {
      const [result, count] = await this.fileRepo.findAndCount();
      return {
        list: result,
        total: count,
      };
    } else {
      const [result, count] = await this.sharedUserRepo.findAndCount({
        take: limit,
        where: {
          //relation with file
          file: {
            is_sharing:0
          },
          user_id: user.id,
          is_shared: 1
        },
        
        relations: ["file"],
        skip: (page >= 0 ? page : 0) * limit,
        order: {
          updatedAt: "DESC",
        }
        
        
      });
      return {
        list: result,
        total: count,
      };
    }
    // return this.sharedUserRepo.find({ relations: ["user",'owner','file'] });
  }
  async getSharedFile(id, user: User) {
    const shared_file: any = await this.fileRepo.findOne({
      where: {
        id
      }
    });
    const shared_users: any = await this.sharedUserRepo.find({
      where: {
        file_id: id,
        // initiator_id:user.id
      },
      relations: ["user"]
    });
    const comments: any = await this.commentRepo.find({
      where: {
        file_id: id,
      },
      relations: ["user", 'file']
    });
    return { shared_users, comments, shared_file };

    // return this.sharedUserRepo.find({ relations: ["user",'owner','file'] });
  }

  async getSharedWithMeFile(id, user: User) {

    const shared_file: any = await this.sharedUserRepo.find({
      where: {
        file_id: id,
        user_id: user.id,
        is_shared: 1
      },
      relations: ["user", 'file']
    });
    return { shared_file };

    // return this.sharedUserRepo.find({ relations: ["user",'owner','file'] });
  }

  async getOhterUserFile(data: any) {

    return  await this.sharedUserRepo.findOne({
      where: {
        file_id: data.file_id,
        other_user_email: data.email,
        other_user_name: data.name
      },
      relations: ['file']
    });
    // return { shared_file };
  }

  async getInitiatorFileDetails(id, user: User) {

    const shared_file: any = await this.sharedUserRepo.find({
      where: {
        file_id: id,
        initiator_id: user.id,
      },
      relations: ["user", 'file']
    });
    return { shared_file };

    // return this.sharedUserRepo.find({ relations: ["user",'owner','file'] });
  }
  async findOne(id: string, signService: SignatureService) {
    try {
      const file = await this.fileRepo.findOne(id);
      if (!file) {
        throw new NotFoundException('No File Found');
      }
      const signatures = await signService.getFileSignatures(id);
      return { ...file, signatures };
    } catch (error) {
      throw new NotFoundException('No File Found');
    }
  }
  async getfile(fileId: string) {
    try {
      const file = await this.fileRepo.findOne(fileId);
      if (!file) {
        throw new NotFoundException('No File Found');
      }
      return file;
    } catch (error) {
      throw new NotFoundException('No File Found');
    }
  }

  async updateFileStatus(updateFileManagementDto: UpdateFileManagementDto,user:User) {
    const file = await this.fileRepo.update(
      { id: updateFileManagementDto.file_id }, {
      status: updateFileManagementDto.status == 'approved' ? FileStatus.Approved
        : (updateFileManagementDto.status == 'completed' ? FileStatus.Completed : (updateFileManagementDto.status == 'inactive' ? FileStatus.Inactive : (updateFileManagementDto.status == 'inprogress' ? FileStatus.InProgress :FileStatus.Pending)))
    });

    const shared_users = await this.sharedUserRepo.find({
      where: {
        file_id: updateFileManagementDto.file_id,
      }
    });
    console.log('user',user);
    
    // return shared_users;
    const notify_data_arr = [];
    await shared_users.forEach(async(shared_user_data:any, i) => {
     
      const notify_data = {
        send_to: shared_user_data.user_id,
        send_by: user.id,
        status: 9,
        is_unread: 1,
        message: updateFileManagementDto.status,
        file_id: updateFileManagementDto.file_id
      };
      notify_data_arr.push(notify_data);
    
   
    });
    // return notify_data_arr;
   
    await this.notifyRepo.save(notify_data_arr)
    return file;
  }

  async shareFile(id: string, shareFileDto: ShareFileDto, user: User) {
    const { comment, startDate, endDate } = shareFileDto;
    const file = await this.getfile(id);

    file.startDate = startDate;
    file.endDate = endDate;
    await file.save();
    return file;
  }

  async updateFileRequestStatus(request: UpdateFileRequestDto, user: User) {
    const shared_user = await this.sharedUserRepo.findOne({
      file_id: request.file_id,
      user_id: user.id
    });
    const file = await this.fileRepo.findOne({
      id: request.file_id,
    });
    if (file) {
      file.is_editable = request.status == 2 ? 1 : 0;
      await file.save();
    }
    if (shared_user) {
      // replace new file name with old file name
      shared_user.is_accept = request.status;
      await shared_user.save();
    }

    await this.notifyRepo.save([{
      send_to: request.initiator_id,
      send_by: user.id,
      status: request.status == 1 ? 4 : 5,
      is_unread: 1,
      file_id: request.file_id,
      request_sent: 1,
      message: request.message
    }])
    await this.notifyRepo.update(
      { id: request.notification_id }, {
      request_sent: 0
    });
    return file;
  }

  remove(id: string) {
    return `This action removes a #${id} fileManagement`;
  }

  // async search(SearchFileRequestDto: SearchFileRequestDto, limit, page, user: User) {
  //   //  const limit:any = parseInt(limit) || 10;
  //   //   page = parseInt(page) - 1 || 0;
  //   console.log('initiator id', user.id);
  //   console.log('SearchFileRequestDto.role', SearchFileRequestDto.role);

  //   const keyword = SearchFileRequestDto.keyword;
  //   const where: any = {
  //     // initiator_id :user.id,
  //     // originalFileName: Like(`%${keyword}%`),

  //     // AND
  //     initiator: {
  //       id: 1,
  //       isActive: 0
  //       // role: SearchFileRequestDto.role ? SearchFileRequestDto.role : In(['User', 'Admin']),

  //     },



  //   }
  //   // if(SearchFileRequestDto.date) {
  //   //    // AND
  //   //    console.log(555);

  //   //   where.startDate = Like(`%${SearchFileRequestDto.date}%`)
  //   // }
  //   // if(SearchFileRequestDto.status) {
  //   //    // AND
  //   //    console.log(5525);
  //   //   where.status =FileStatus.Pending
  //   // }

  //   const [result, count] = await this.fileRepo.findAndCount({
  //     relations: ['initiator'],
  //     // join: { alias: 'user', innerJoin: { users: 'user.id' } },
  //     // take: limit,
  //     // loadEagerRelations:false,
  //     // join: {
  //     //   alias: 'user',
  //     //   innerJoinAndSelect : {
  //     //     initiator_id: "user.id"
  //     //   }
  //     // },
  //     where: {
  //       // initiator_id:1,
  //       initiator: {
  //         id: 1,
  //         isActive: 0,
  //         role: 'User'
  //       },

  //       // initiator: 1
  //     },
  //     // skip: (page >= 0 ? page : 0) * limit,
  //   });


  //   return {
  //     list: result,
  //     total: count,
  //   };
  // }

  // async search(SearchFileRequestDto: SearchFileRequestDto, limit, page, user: User) {
  //   //  const limit:any = parseInt(limit) || 10;
  //    //   page = parseInt(page) - 1 || 0;
  //    const keyword = SearchFileRequestDto.keyword;
  //   const where:any = {
  //     originalFileName: Like(`%${keyword}%`),
  //     //  initiator_id:user.id,
  //      // AND
  //     initiator: {
  //       role: SearchFileRequestDto.role ? SearchFileRequestDto.role : In([UserRole.Admin, UserRole.User]),
  //     },
  //    }
  //    if(SearchFileRequestDto.date) {
  //      // AND
  //      where.startDate = Like(`%${SearchFileRequestDto.date}%`)
  //    }
  //    if(SearchFileRequestDto.status) {
  //       // AND
  //       where.status =SearchFileRequestDto.status
  //    }
  // // return where;
  //    const result = await this.fileRepo.find({
  //       relations: ['initiator'],

  //       where:where
  //       // where: {
  //       //   originalFileName: Like(`%${keyword}%`),
  //       //   // initiator_id:user.id,
  //       //   status:SearchFileRequestDto.status,
  //       //   // startDate: Like(`%${SearchFileRequestDto.date}%`)
  //       //   initiator: {
  //       //     // role: UserRole.Admin,
  //       //     role: In([UserRole.Admin, UserRole.User]),
  //       //   },
  //       // }
  //       // take: limit,
  //       // where: where,
  //       // skip: (page >= 0 ? page : 0) * limit,
  //     });
  
   
  //     return {
  //       list: result,
  //       // total: count,
  //     };
  //   }
  async search(SearchFileRequestDto: SearchFileRequestDto, limit, page, user: User) {
    //  const limit:any = parseInt(limit) || 10;
     //   page = parseInt(page) - 1 || 0;
     console.log('user id',user.id);
     
     const keyword = SearchFileRequestDto.keyword;
    const where:any = {
      originalFileName: Like(`%${keyword}%`),
      // initiator_id:user.id,
       // AND
       
      initiator: {
        role: SearchFileRequestDto.role ? SearchFileRequestDto.role :  In([UserRole.Admin, UserRole.User]),
        // id:user.id
      },
     }
     if(SearchFileRequestDto.date) {
       // AND
       where.startDate = Like(`%${SearchFileRequestDto.date}%`)
     }
     if(SearchFileRequestDto.status) {
        // AND
        where.status =SearchFileRequestDto.status
     }
     if(user.role != UserRole.Admin) {
      where.initiator.id =user.id
     }
  
     const [result, count] = await this.fileRepo.findAndCount({
        relations: ['initiator'],
        take: limit,
        where: where,
        skip: (page >= 0 ? page : 0) * limit,
      });
  
  
      return {
        list: result,
        total: count,
      };
    }

    async searchSharedWithMe(SearchFileRequestDto: SearchFileRequestDto, limit, page, user: User) {
       console.log('user id',user.id);
       const keyword = SearchFileRequestDto.keyword;
       const shared_user_files = await this.sharedUserRepo.find({
        select: ["file_id"],
        where: {
          //relation with file
          file: {
            is_sharing:0
          },
          user_id: user.id,
          is_shared: 1
        },
        relations: ["file"],
      });
      let shared_files_id_arr = [];
      shared_user_files.forEach(element => {
        shared_files_id_arr.push(element.file_id);
      });

      const where:any = {
        originalFileName: Like(`%${keyword}%`),
        id:In(shared_files_id_arr),
    
        initiator: {
          role: SearchFileRequestDto.role ? SearchFileRequestDto.role :  In([UserRole.Admin, UserRole.User]),
        },
       }
       if(SearchFileRequestDto.date) {
         // AND
         where.startDate = Like(`%${SearchFileRequestDto.date}%`)
       }
       if(SearchFileRequestDto.status) {
          // AND
          where.status =SearchFileRequestDto.status
       }
     
       const [result, count] = await this.fileRepo.findAndCount({
          relations: ['initiator'],
          take: limit,
          where: where,
          skip: (page >= 0 ? page : 0) * limit,
        });
    
    
        return {
          list: result,
          total: count,
        };
      }

    async searchSharingFile(SearchFileRequestDto: SearchFileRequestDto, limit, page, user: User) {
      //  const limit:any = parseInt(limit) || 10;
       //   page = parseInt(page) - 1 || 0;
       const keyword = SearchFileRequestDto.keyword;
      const where:any = {
        originalFileName: Like(`%${keyword}%`),
        is_sharing:1,
        //  initiator_id:user.id,
         // AND
        initiator: {
          role: SearchFileRequestDto.role ? SearchFileRequestDto.role :  In([UserRole.Admin, UserRole.User]),
        },
       }
       if(SearchFileRequestDto.date) {
         // AND
         where.startDate = Like(`%${SearchFileRequestDto.date}%`)
       }
       if(SearchFileRequestDto.status) {
          // AND
          where.status =SearchFileRequestDto.status
       }
    
       const [result, count] = await this.fileRepo.findAndCount({
          relations: ['initiator'],
          take: limit,
          where: where,
          skip: (page >= 0 ? page : 0) * limit,
        });
    
    
        return {
          list: result,
          total: count,
        };
      }
  
  


  async deleteFile(id: number,user:User): Promise<void> {
    const deleteResponse = await this.fileRepo.softDelete(id);
    if (!deleteResponse.affected) {
      throw new NotFoundException(id);
    }else {
      const deleteResponse = await this.fileRepo.update({
        id
      },{
        status:FileStatus.Deleted
      });
    }
  }

  // async saveFileData(filedata: any) {
  //   // const result: any = await this.userRepo.userRegister(createUserDto, false);
  //   const result: any = await this.userRepo.userRegister(createUserDto, false);
  //   delete result.resetPasswordToken;
  //   delete result.password;
  //   delete result.salt;
  //   delete result.deletedAt;

  //   return result;
  // }
}
