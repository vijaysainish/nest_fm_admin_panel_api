import {
    BaseEntity,
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    ObjectIdColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    RelationId,
    JoinColumn
  } from 'typeorm';
  import { FileStatus } from './../../../enums/file-status.enums';

  import {User} from  './../../users/entities/user.entity';
  import {ShareFiles} from  './../../file-management/entities/file-management.entity';
  
  @Entity()
  export class FileComment extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    // @Column()
    // name: string;
    @Column()
    file_id: number;
   
  
    @Column({ nullable: true })
    user_id: number;

    @Column({ nullable: true, length: 500  })
    comment: string;

    @CreateDateColumn()
    createdAt: Date;

  


    @ManyToOne(type => User, user => user.id,{
      onDelete: "CASCADE"
    })
     @JoinColumn({ name: "user_id" })
     user: User;

     @ManyToOne(type => ShareFiles, file => file.id,{
      onDelete: "CASCADE"
    })
     @JoinColumn({ name: "file_id" })
     file: ShareFiles;
  }
  