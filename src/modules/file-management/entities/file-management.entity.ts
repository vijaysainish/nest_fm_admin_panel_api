import { User } from 'src/modules/users/entities/user.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ObjectIdColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
  OneToOne
} from 'typeorm';
import { FileStatus } from './../../../enums/file-status.enums';
import { FileComment } from './file-comment.entity';
import { SharedUser } from './user-shared-files.entity';

@Entity()
export class ShareFiles extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  initiator_id: number;

  // @Column()
  // name: string;

  // @Column()
  // initiator_id: number;

  // @Column()
  // user_ids: string;

  // @Column({ nullable: true })
  // signed_user_ids: string;



  @Column()
  fileName: string;

  @Column()
  originalFileName: string;

  @Column({ nullable: true })
  startDate: Date;

  @Column({ nullable: true })
  endDate: Date;

  @Column({ type: 'enum', enum: FileStatus, default: FileStatus.Pending })
  status: FileStatus;

  

  @Column({ nullable: true })
  is_sharing: number;

  @Column({ default: 0 })
  is_editable: number;


  
  


  // @Column()
  // uploadedBy: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt: Date;

  @ManyToOne(type => User, user => user.id,{
    onDelete: "CASCADE"
  })
   @JoinColumn({ name: "initiator_id" })
   initiator: User[];

   @OneToMany(type => FileComment, comment => comment.file,{
    onDelete: "CASCADE"
  })
   @JoinColumn({ name: "id" })
   comment: FileComment[];

   @OneToMany(type => SharedUser, shared_user => shared_user.file,{
    onDelete: "CASCADE"
  })
   @JoinColumn({ name: "id" })
   shared_users: SharedUser[];

}
