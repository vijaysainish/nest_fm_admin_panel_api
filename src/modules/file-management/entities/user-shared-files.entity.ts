import {
    BaseEntity,
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    ObjectIdColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    RelationId,
    JoinColumn
  } from 'typeorm';
  import { FileStatus } from './../../../enums/file-status.enums';

  import {User} from  './../../users/entities/user.entity';
  import {ShareFiles} from  './../../file-management/entities/file-management.entity';
  
  @Entity()
  export class SharedUser extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    // @Column()
    // name: string;
    @Column()
    file_id: number;
    @Column()
    initiator_id: number;
  
    @Column({ nullable: true })
    user_id: number;

    @Column({ nullable: true })
    other_user_name: string;

    @Column({ nullable: true })
    other_user_email: string;
  
    // @Column({ nullable: true })
    // signed_user_ids: string;
  
  
  
    // @Column()
    // sign_status: string;
  
    @Column({ type: 'enum', enum: FileStatus, default: FileStatus.Pending })
    sign_status: FileStatus;

    @Column({ default: 0})
    is_shared: number;

    @Column({ default: 0,comment: '1 for accepted, 2 for rejected'})
    is_accept: number;

    @Column({ default: 0 })
    is_locked: number;
  
    // @Column({ nullable: true })
    // comment: string;
  
    // @Column()
    // uploadedBy: string;
  
    // @CreateDateColumn()
    // createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;
  
    // @DeleteDateColumn({ nullable: true })
    // deletedAt: Date;

    @ManyToOne(type => User, user => user.id,{
      onDelete: "CASCADE"
    })
     @JoinColumn({ name: "user_id" })
     user: User;

     @ManyToOne(type => User, user => user.id,{
      onDelete: "CASCADE"
    })
     @JoinColumn({ name: "initiator_id" })
     initiator: User;

     @ManyToOne(type => ShareFiles, file => file.id,{
      onDelete: "CASCADE"
    })
     @JoinColumn({ name: "file_id" })
     file: ShareFiles;
  }
  