import { InternalServerErrorException } from '@nestjs/common';
import { FileStatus } from 'src/enums/file-status.enums';
import { EntityRepository, Repository } from 'typeorm';
import { CreateFileManagementDto } from '../dto/create-file-management.dto';
import { UpdateFileManagementDto } from '../dto/update-file-management.dto';
import { ShareFiles } from './../entities/file-management.entity';

@EntityRepository(ShareFiles)
export class FileManagementRepository extends Repository<ShareFiles> {
  async newFile(
    createFileManagementDto: CreateFileManagementDto,
    userId: number,is_sharing=0
  ) {
    const file_data = createFileManagementDto.FileData;
    const newFile = new ShareFiles();
    newFile.fileName = file_data.filename;
    newFile.originalFileName = file_data.originalFileName;
    newFile.startDate = file_data.startDate;
    newFile.endDate = file_data.endDate;
    newFile.is_sharing = is_sharing;
    newFile.is_editable = is_sharing ? 1 :0;
    newFile.status = is_sharing ? FileStatus.Pending : FileStatus.InProgress
    newFile.initiator_id = userId;
    // newFile.comment = file_data.comment;
    // newFile.initiator_id = userId;
    // newFile.fileName = name;
    try {
      await newFile.save();
      return newFile;
    
    } catch (e) {
      console.log(e);
      throw new InternalServerErrorException('Something Went wrong');
    }
  }

  async updateFile(
    updateFileManagementDto: UpdateFileManagementDto,
    userId: number,
  ) {
    const file_id = updateFileManagementDto.file_id;
    const filename = updateFileManagementDto.filename;
    const newFile = new ShareFiles();
   
    try {
      await newFile.save();
      return newFile;
    
    } catch (e) {
      console.log(e);
      throw new InternalServerErrorException('Something Went wrong');
    }
  }
}
