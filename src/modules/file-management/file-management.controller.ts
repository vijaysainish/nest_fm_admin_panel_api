import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

import { GetUser } from 'src/decorators/getUser.decorator';
import { JwtAuthGuard } from '../auth/auth.guard';
import { SignatureService } from '../signature/signature.service';
import { User } from '../users/entities/user.entity';
import { CreateFileManagementDto } from './dto/create-file-management.dto';
import { ShareFileDto } from './dto/share-file.dto';
import { UpdateFileManagementDto } from './dto/update-file-management.dto';
import { FileManagementService } from './file-management.service';

import {imageFileFilter,editFileName} from './../middleware/image.middleware'
import { RoleGuard } from '../auth/role.guard';
import { UpdateFileRequestDto } from './dto/update-file-request.dto copy';
import { SearchFileRequestDto } from './dto/search.dto';
import { UpdateOtherUserFiletDto } from './dto/update-other-user-file.dto';
import { saveEditFileDto } from './dto/save-edit-file.dto';

@Controller('file-management')
export class FileManagementController {
  constructor(
    private readonly fileManagementService: FileManagementService,
    private readonly signService: SignatureService,
  ) {}

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post()
  create(
    @Body() createFileManagementDto: CreateFileManagementDto,
    @GetUser() user: User,
  ) {
    return this.fileManagementService.create(createFileManagementDto, user);
  }
  @UseGuards(JwtAuthGuard,RoleGuard)
  @Get('files-list')
  getFiles(@GetUser() user: User,@Query('page') page, @Query('limit') limit) {
    // console.log(user);
    return this.fileManagementService.getFiles(user,page, limit);
  }
  @UseGuards(JwtAuthGuard,RoleGuard)
  @Get('sharing-files')
  getSharingFiles(@GetUser() user: User,@Query('page') page, @Query('limit') limit) {
    // console.log(user);
    return this.fileManagementService.getSharingFiles(user,page, limit);
  }
  @UseGuards(JwtAuthGuard,RoleGuard)
  @Get('files-shared-with-me')
  getSharedWithMeFiles(@GetUser() user: User,@Query('page') page, @Query('limit') limit) {
    // console.log(user);
    return this.fileManagementService.getSharedWithMeFiles(user,page, limit);
  }

  
  @UseGuards(JwtAuthGuard,RoleGuard)
  @Get('shared-file')
  getSharedFile(@Query('id') id: any,@GetUser() user: User,) {
    // return id;
    // if (user.role !== UserRole.Admin)
    //   throw new UnauthorizedException(
    //     'You are not Authorized to edit this user',
    //   );
    return this.fileManagementService.getSharedFile(id,user);
  }
  @UseGuards(JwtAuthGuard,RoleGuard)
  @Get('shared-with-me-file')
  getSharedWithMeFile(@Query('id') id: any,@GetUser() user: User,) {
    return this.fileManagementService.getSharedWithMeFile(id,user);
  }
  @Get('other-user-file')
  getOhterUserFile(@Query('name') name: any,@Query('email') email: any,@Query('id') file_id: any) {
    const data = {
      file_id,name,email
    }
    return this.fileManagementService.getOhterUserFile(data);
  }
  @UseGuards(JwtAuthGuard,RoleGuard)
  @Get('initiator-file-details')
  getInitiatorFileDetails(@Query('id') id: any,@GetUser() user: User,) {
    return this.fileManagementService.getInitiatorFileDetails(id,user);
  }
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fileManagementService.findOne(id, this.signService);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('share/:id')
  sharefile(
    @Param('id') id: string,
    @Body() shareFileDto: ShareFileDto,
    @GetUser() user: User,
  ) {
    return this.fileManagementService.shareFile(id, shareFileDto, user);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('update-file-request-status')
  acceptRejectFileRequest(
   @Body() UpdateRequestDto:UpdateFileRequestDto,
    @GetUser() user: User,
  ) {
    return this.fileManagementService.updateFileRequestStatus(UpdateRequestDto, user);
  }
  @UseGuards(JwtAuthGuard,RoleGuard)
  @Patch('update-file-status')
  updateFileStatus(
    @Body() updateFileManagementDto: UpdateFileManagementDto,@GetUser() user: User
  ) {
    return this.fileManagementService.updateFileStatus(updateFileManagementDto,user);
  }
  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.fileManagementService.remove(id);
  // }
  // @UseGuards(JwtAuthGuard)
  @Post('upload')
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: './uploads',
      // filename: editFileName,
    }),
    // fileFilter: imageFileFilter,
  }))
  uploadFile(@UploadedFile() file: Express.Multer.File) {
    // console.log(file);
    // return this.usersService.searchUser(name, email);
    return file;
   
  }
  
  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('add-file')
  addFile(@Body() filedata: CreateFileManagementDto, @GetUser() user: User) {
    // return filedata.FileData;
    // if (user.role !== UserRole.Admin)
    //   throw new UnauthorizedException(
    //     'You are not Authorized to edit this user',
    //   );
    return this.fileManagementService.create(filedata,user);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('save-file')
  saveFile(@Body() filedata: CreateFileManagementDto, @GetUser() user: User) {
  
    return this.fileManagementService.saveFile(filedata,user);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('search-listing-file')
  search(@Body() searchdata: SearchFileRequestDto, @GetUser() user: User) {
    // return filedata.FileData;
    // if (user.role !== UserRole.Admin)
    //   throw new UnauthorizedException(
    //     'You are not Authorized to edit this user',
    //   );
    return this.fileManagementService.search(searchdata,10,0,user);
  }
  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('search-shared-with-me')
  searchSharedWithMe(@Body() searchdata: SearchFileRequestDto, @GetUser() user: User) {
    // return filedata.FileData;
    // if (user.role !== UserRole.Admin)
    //   throw new UnauthorizedException(
    //     'You are not Authorized to edit this user',
    //   );
    return this.fileManagementService.searchSharedWithMe(searchdata,10,0,user);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('search-sharing-file')
  searchSharingFile(@Body() searchdata: SearchFileRequestDto, @GetUser() user: User) {
    return this.fileManagementService.searchSharingFile(searchdata,10,0,user);
  }


  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('update-file')
  updateFile(@Body() filedata: UpdateFileManagementDto, @GetUser() user: User) {
    // return 1;
    return this.fileManagementService.updateFile(filedata,user);
  }
 
  @Post('update-other-user-file')
  updateOtherUserFile(@Body() filedata: UpdateOtherUserFiletDto) {
    // return 1;
    return this.fileManagementService.updateOtherUserFile(filedata);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('edit-reshare-file')
  editReshareFile(@Body() filedata: UpdateFileManagementDto, @GetUser() user: User) {
    // return 1;
    return this.fileManagementService.editReshareFile(filedata,user);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('edit-sharing-file')
  editSharingFile(@Body() filedata: saveEditFileDto, @GetUser() user: User) {
    // return 1;
    return this.fileManagementService.editSharingFile(filedata,user);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Post('share-file')
  shareFileFromList(@Body() filedata: ShareFileDto, @GetUser() user: User) {
    // return 1;
    return this.fileManagementService.shareFileFromList(filedata,user);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Delete('delete-file')
  deleteFile(@Body() data: any, @GetUser() user: User) {
    // return data.file_id;
    return this.fileManagementService.deleteFile(data.file_id,user);
  }

   @Get('file/:imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    return res.sendFile(image, { root: './uploads' });
  }
 
}
