import { Optional } from '@nestjs/common';
import { PartialType } from '@nestjs/mapped-types';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { text } from 'stream/consumers';
import { CreateFileManagementDto } from './create-file-management.dto';

export class UpdateFileRequestDto extends PartialType(
  CreateFileManagementDto,
) {
  @IsNumber()
  notification_id: number;
  @IsNumber()
  file_id: number;
  @IsNumber()
  status: number;

  @IsNumber()
  initiator_id: number;

  @IsString()
  @IsOptional()
  message: string;
}
