import { Type } from 'class-transformer';
import { IsArray, IsDate, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
 class SharedUser {
  @IsArray()
  @IsOptional()
  new_user_ids:Array<any>;

  @IsArray() 
  @IsOptional()
  rm_user_ids:Array<any>;
  
}
 class FileData {
  @IsString()
  filename: string;
  @IsNumber()
  file_id: number;

}

export class saveEditFileDto {

  @Type(() => SharedUser)
  @ValidateNested()
  readonly SharedUser: SharedUser

  
  @Type(() => FileData)
  @ValidateNested()
  readonly FileData: FileData
}
