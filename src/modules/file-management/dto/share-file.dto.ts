import { IsDate, IsDateString, IsNumber, isNumber, IsOptional, IsString } from 'class-validator';

export class ShareFileDto {
  @IsNumber()
  @IsOptional()
  file_id: number;
  @IsString()
  @IsOptional()
  comment: string;
  @IsOptional()
  comment_id: number;

  @IsDateString()
  startDate: Date;

  @IsDateString()
  endDate: Date;
}
