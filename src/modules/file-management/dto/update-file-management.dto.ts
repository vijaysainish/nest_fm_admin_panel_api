import { Optional } from '@nestjs/common';
import { PartialType } from '@nestjs/mapped-types';
import { IsNumber, isNumber, IsOptional, IsString } from 'class-validator';
import { CreateFileManagementDto } from './create-file-management.dto';

export class UpdateFileManagementDto extends PartialType(
  CreateFileManagementDto,
) {

  @IsString()
  @IsOptional()
  filename: string;
  @IsString()
  @IsOptional()
  status: string;
  
  @IsNumber()
  file_id: number;
}
