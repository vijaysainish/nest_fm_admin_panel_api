import { Optional } from '@nestjs/common';
import { PartialType } from '@nestjs/mapped-types';
import { IsNumber, isNumber, IsOptional, IsString } from 'class-validator';
import { CreateFileManagementDto } from './create-file-management.dto';

export class UpdateOtherUserFiletDto extends PartialType(
  CreateFileManagementDto,
) {

  @IsString()
  @IsOptional()
  filename: string;
  @IsString()
  @IsOptional()
  status: string;
  
  @IsNumber()
  file_id: number;

  @IsString()
  name: string;

  @IsString()
  email: string;
}
