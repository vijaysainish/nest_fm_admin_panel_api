import { Type } from 'class-transformer';
import { IsArray, IsDate, IsString, ValidateNested } from 'class-validator';
 class SharedUser {
  @IsArray() user_ids:Array<any>;
  // readonly score:number
  // readonly description:string
  // readonly dateOfCreation:Date
}
 class FileData {
  @IsString()
  filename: string;
  @IsString()
  originalFileName: string;

  @IsDate()
  @Type(() => Date)
  startDate: Date;

  @IsDate()
  @Type(() => Date)
  endDate: Date;

 
  comment: any;
}

export class CreateFileManagementDto {

  @Type(() => SharedUser)
  @ValidateNested()
  readonly SharedUser: SharedUser

  
  @Type(() => FileData)
  @ValidateNested()
  readonly FileData: FileData



  

}
