import { IsString } from 'class-validator';
export class CreateFileCommentDto {
  @IsString()
  comment: string;
}
