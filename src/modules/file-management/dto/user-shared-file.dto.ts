import { IsString } from 'class-validator';
export class CreateFileManagementDto {
  @IsString()
  name: string;
}
