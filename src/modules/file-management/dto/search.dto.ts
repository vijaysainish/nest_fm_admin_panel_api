

import { Type } from 'class-transformer';
import { IsDate, IsNumber, IsOptional, IsString } from 'class-validator';


export class SearchFileRequestDto {

  @IsString()
  @IsOptional()
  keyword: string;

  @IsString()
  @IsOptional()
  role: string;

  @IsString()
  @IsOptional()
  status: string;

  @IsDate()
  @IsOptional()
  @Type(() => Date)

  date: Date;

  @IsDate()
  @IsOptional()
  @Type(() => Date)

  start_date: Date;

  @IsOptional()
  @IsDate()
  @Type(() => Date)

  end_date: Date;
}
