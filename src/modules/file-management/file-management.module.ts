import { UserRepository } from 'src/modules/users/repository/user.repository';
import { UsersService } from './../users/user.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SignatureRepository } from './../signature/repository/signature.repository';
import { SignatureService } from './../signature/signature.service';
import { FileManagementController } from './file-management.controller';
import { FileManagementService } from './file-management.service';
import { FileManagementRepository } from './repository/file-management.repository';
import {SharedUserRepository  } from './repository/user-shared-files.repositroy';
import {CommentRepository  } from './repository/file-comment.repositroy';
import { NotificationRepository } from '../notifications/repository/notifications.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      FileManagementRepository,
      SignatureRepository,
      UserRepository,
      SharedUserRepository,
      CommentRepository,
      NotificationRepository
    ]),
  ],
  controllers: [FileManagementController],
  providers: [FileManagementService, SignatureService, UsersService],
})
export class FileManagementModule {}
