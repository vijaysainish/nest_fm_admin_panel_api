import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateLogoDto } from '../dto/logo.dto';
import { Logo } from './../entities/logo.entity';

@EntityRepository(Logo)
export class LogoRepository extends Repository<Logo> {

    async createLogo(
        createLogoDto: CreateLogoDto,
      ) {
        const data = createLogoDto;
        const logo = new Logo();
        logo.name = data.name;
        logo.image = data.image;
    
        try {
          await logo.save();
          return logo;
        
        } catch (e) {
          console.log(e);
          throw new InternalServerErrorException('Something Went wrong');
        }
      }

}
