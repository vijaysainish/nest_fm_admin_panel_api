import { Body, Controller, Get, Patch } from '@nestjs/common';
import { AdminService } from './admin.service';
import { CreateLogoDto } from './dto/logo.dto';

@Controller('admin')
export class AdminController {

    constructor(private readonly adminService: AdminService) {}


    @Get('logo')
    getLogo() {
    //  return 1;
      return this.adminService.getLogo();
    }

    @Patch('update-logo-info')
    updateLogoInfo( @Body() data: CreateLogoDto) {
      return this.adminService.updateLogoInfo(data);
    }
  
}
