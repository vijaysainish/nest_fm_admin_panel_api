import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLogoDto } from './dto/logo.dto';
import { LogoRepository } from './repository/logo.repository';

@Injectable()
export class AdminService {
    constructor(
        @InjectRepository(LogoRepository) private logoRepo: LogoRepository,
      ) {}
      async getLogo() {
    const result = await this.logoRepo.findOne();
    // const result = await this.logoRepo.findOne({role:UserRole.Admin}, {
    //   select: [
    //     'logo',
    //   ],
    // });
    // if (!result) throw new NotFoundException('No User Found');
    return result;
  }

  async updateLogoInfo(createLogoDto:CreateLogoDto) {
    const {
    image,
    name
    } = createLogoDto;
    // const updated_user = await this.logoRepo.createLogo(createLogoDto);
  const updated_user = await this.logoRepo.update({id:1},
    {
        name:name,
        image:image
    });

    
    return updated_user;
    }

}
