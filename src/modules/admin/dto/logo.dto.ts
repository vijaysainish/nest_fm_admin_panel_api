import { IsOptional, IsString } from 'class-validator';
export class CreateLogoDto {
  @IsString()
  @IsOptional()
  name: string;


  @IsString()
  @IsOptional()
  image: string;
}
