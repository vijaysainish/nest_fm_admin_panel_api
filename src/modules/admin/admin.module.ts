import { Module } from '@nestjs/common';
import { AdminService } from './admin.service';



import { TypeOrmModule } from '@nestjs/typeorm';
import { LogoRepository } from './repository/logo.repository';
import { AdminController } from './admin.controller';


@Module({
  imports: [TypeOrmModule.forFeature([LogoRepository])],
  controllers: [AdminController],
  providers: [AdminService],
})
export class AdminModule {}
