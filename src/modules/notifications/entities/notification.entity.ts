import { ShareFiles } from 'src/modules/file-management/entities/file-management.entity';
import { User } from 'src/modules/users/entities/user.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ObjectIdColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn
} from 'typeorm';

@Entity()
export class Notification extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: any;
  @Column()
  file_id: number;

  @Column({ nullable: true })
  send_by: number;
  @Column({ nullable: true })
  send_to: number;



  @Column({comment: '1 for shared,2 for signed,3 for next user time to submit sign,4 for user accept the file , 5 for  user reject the file,6 for file edited, 7 for reshare edit file',default:0})
  status: number;

  @Column({default:0})
  request_sent: number;

 

  @Column({ nullable: true,length:500 })
  message: string;

  @Column({ default: 0 })
  is_unread: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;


  @ManyToOne(type => User, user => user.id,{
    onDelete: "CASCADE"
  })
   @JoinColumn({ name: "send_by" })
   send_by_user: User[];

   @ManyToOne(type => User, user => user.id,{
    onDelete: "CASCADE"
  })
   @JoinColumn({ name: "send_to" })
   send_to_user: User[];

   @ManyToOne(type => ShareFiles, file => file.id,{
    onDelete: "CASCADE"
  })
   @JoinColumn({ name: "file_id" })
   shared_file: ShareFiles[];
}
