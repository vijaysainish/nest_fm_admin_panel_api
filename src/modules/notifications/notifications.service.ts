import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';

import { CreateNotificationDto } from './dto/create-notification.dto';
import { UpdateNotificationDto } from './dto/update-notification.dto';
import { NotificationRepository } from './repository/notifications.repository';

@Injectable()
export class NotificationsService {
  constructor( @InjectRepository(NotificationRepository)
  private notifyRepo: NotificationRepository) {
   
  }
  create(createNotificationDto: CreateNotificationDto) {
    return 'This action adds a new notification';
  }

  findAll() {
    return `This action returns all notifications`;
  }
  async getUserNotifications( user: User,limit, page) {
    console.log(user);
    limit = parseInt(limit) || 10;
    page = parseInt(page) - 1 || 0;
    const [result, count] = await this.notifyRepo.findAndCount({
      where: {
        send_to: user.id
      },
      order: {
        createdAt: "DESC",
    },
      relations: ["send_by_user",'send_to_user','shared_file'],
      take: limit,
      skip: (page >= 0 ? page : 0) * limit,
    });
    const unread_msg =  await this.notifyRepo.count({
      where: {
        send_to: user.id,
        is_unread:1
      }
    });

    return {
      list: result,
      total:count,
      unread_msg
    };
  }

  findOne(id: number) {
    return `This action returns a #${id} notification`;
  }
 async readNotifications(user:User) {
    const notify_users = await this.notifyRepo.update(
     {send_to:user.id},{
      is_unread: 0
      });

    return notify_users;
  }

  update(id: number, updateNotificationDto: UpdateNotificationDto) {
    return `This action updates a #${id} notification`;
  }

  remove(id: number) {
    return `This action removes a #${id} notification`;
  }
}
