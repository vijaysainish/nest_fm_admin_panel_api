import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Res,
  UnauthorizedException,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { GetUser } from 'src/decorators/getUser.decorator';
import { UserRole } from 'src/enums/user.enums';
import { JwtAuthGuard } from './../auth/auth.guard';
import { RoleGuard } from './../auth/role.guard';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UsersService } from './user.service';

@Controller('user')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  // @UseGuards(JwtAuthGuard)
  @Post('add')
  create(@Body() createUserDto: CreateUserDto, @GetUser() user: User) {
    // if (user.role !== UserRole.Admin)
    //   throw new UnauthorizedException(
    //     'You are not Authorized to edit this user',
    //   );
    return this.usersService.create(createUserDto);
  }

  @Post('add-by-csv')
  createByCSV(@Body() createUserDto: CreateUserDto[], @GetUser() user: User) {
    // if (user.role !== UserRole.Admin)
    //   throw new UnauthorizedException(
    //     'You are not Authorized to edit this user',
    //   );
    return this.usersService.createByCSV(createUserDto);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Get('list')
  findAll(@Query('page') page, @Query('limit') limit, @Query('search') search) {
    return this.usersService.findAll(page, limit, search);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Get('all_user_list')
  getAll() {
    return this.usersService.getUsers();
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Get('all-user-emails')
  getEmailsAll() {
    return this.usersService.getUsersEmails();
  }

  @UseGuards(JwtAuthGuard)
  @Get('d/:id')
  findOne(@Param('id') id: string, @GetUser() user: User) {
    if (user.role !== UserRole.Admin)
      throw new UnauthorizedException(
        'You are not Authorized to edit this user',
      );
    return this.usersService.findOne(id);
  }


 

  @UseGuards(JwtAuthGuard)
  @Get()
  getDetails(@GetUser() user: User) {
    return this.usersService.findOne(user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch()
  updateAccount(@GetUser() user: User, @Body() body: UpdateUserDto) {
    return this.usersService.update(user.id, body);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('edit/:id')
  update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
    @GetUser() user: User,
  ) {
    if (user.role !== UserRole.Admin)
      throw new UnauthorizedException(
        'You are not Authorized to edit this user',
      );
    return this.usersService.update(id, updateUserDto);
  }

  @UseGuards(JwtAuthGuard,RoleGuard)
  @Patch('enable-notifications')
  remove(@GetUser() user: User) {
    return this.usersService.changeNotifications(true, user);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('disable-notifications')
  activate(@GetUser() user: User) {
    return this.usersService.changeNotifications(false, user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('search')
  searchUser(@Query('name') name: string, @Query('email') email: string) {
    return this.usersService.searchUser(name, email);
  }
  @UseGuards(JwtAuthGuard,RoleGuard)
  @Patch('update-image')
  updateImage( @Body() filename: UpdateUserDto,@GetUser() user: User) {
    return this.usersService.uploadUserImage(filename, user);
  }

  @Get(':imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    return res.sendFile(image, { root: './uploads/images' });
  }
}
