import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRole } from 'src/enums/user.enums';
import { Like, getRepository, Raw, Equal } from 'typeorm';
import { RoleGuard } from '../auth/role.guard';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UserRepository } from './repository/user.repository';
import * as bcrypt from 'bcrypt';


@Injectable()
export class UsersService {
  constructor(private readonly mailerService: MailerService,
    @InjectRepository(UserRepository) private userRepo: UserRepository,
  ) {}
  async create(createUserDto: CreateUserDto) {
    const result: any = await this.userRepo.userRegister(createUserDto, false);
    delete result.resetPasswordToken;
    delete result.password;
    delete result.salt;
    delete result.deletedAt;
    await this.mailerService
      .sendMail({
        to: result.email,
        // from: 'support@playgroundraters.com',
        subject: 'File Share Registration',
        template: 'src/templates/add-user.pug',
        context: {
          name: result.firstName + ' ' + (result.lastName || ''),
          email: result.email,
          password:createUserDto.password,
          role:createUserDto.isAdmin ? `Admin` : `User`,
          url: createUserDto.isAdmin ?
           `https://dms.miraclechd.co/admin/login` :
            `https://dms.miraclechd.co/auth/login`,
        },
      })
      .then(() => console.log('success'))
      .catch((e) => console.log(e));
    return result;
  }

  async createByCSV(createUserDto: CreateUserDto[]) {
 
      let i = 0;
      for (const iterator of createUserDto) {
          const salt  = await bcrypt.genSalt(11);
          // await this.sleep(300);
          const password = createUserDto[i].password;
          console.log('salt',salt);
          console.log('i',i);
          console.log('password',password);

          
        createUserDto[i].salt = salt;
        createUserDto[i].isActive = createUserDto[i].isActive ?  true:false;
        const passwordHash = (await this.genHash(salt, password)).toString();
        // await this.sleep(300);
        createUserDto[i].password = passwordHash;
        i++;
      }

    const result: any = await this.userRepo.save(createUserDto);
   
    // await this.mailerService
    //   .sendMail({
    //     to: result.email,
    //     // from: 'support@playgroundraters.com',
    //     subject: 'File Share Registration',
    //     template: 'src/templates/add-user.pug',
    //     context: {
    //       name: result.firstName + ' ' + (result.lastName || ''),
    //       email: result.email,
    //       password:createUserDto.password,
    //       role:createUserDto.isAdmin ? `Admin` : `User`,
    //       url: createUserDto.isAdmin ?
    //        `https://dms.miraclechd.co/admin/login` :
    //         `https://dms.miraclechd.co/auth/login`,
    //     },
    //   })
    //   .then(() => console.log('success'))
    //   .catch((e) => console.log(e));
    return result;
  }
  // sleep(ms) {
  //   return new Promise(resolve => setTimeout(resolve, ms));
  // }
  async genHash(salt, password): Promise<string> {
   const pass = password.toString();
    const hash = await bcrypt.hash(pass, salt);
    console.log(hash);
    
    return hash;
  }

  async findAll(page, limit, search) {
    limit = parseInt(limit) || 10;
    page = parseInt(page) - 1 || 0;
    const [result, count] = await this.userRepo.findAndCount({
      take: limit,
      where:[{
        firstName: Raw((alias) => `LOWER(${alias}) Like '%${search}%'`),
        role: 'User'
      },{
        email: Raw((alias) => `LOWER(${alias}) Like '%${search}%'`),
        role: 'User'
      }
    ],
      skip: (page >= 0 ? page : 0) * limit,
      order: {
        createdAt: "DESC",
    }
    });

    
    return {
      list: result,
      total: count,
    };
  }
 
  async getUsers() {
   
    const result = await this.userRepo.find({
      where: {
        role: 'User',
        isActive:1
      },
      select: [
        'id',
        'email',
        'firstName',
        'lastName'
      ]
    });
    return {
      list: result,
    };
  }
  async getUsersEmails() {
   
    const result = await this.userRepo.find({
      select: [
        'email'
      ]
    });
    return {
      list: result,
    };
  }

  async findOne(id: string) {
    const result = await this.userRepo.findOne(id, {
      select: [
        'id',
        'createdAt',
        'email',
        'firstName',
        'isActive',
        'isNotify',
        'lastName',
        'profilePic',
        'role',
        'phoneNumber',
        'deletedAt',
        'updatedAt',
      ],
    });
    if (!result) throw new NotFoundException('No User Found');
    return result;
  }

  async findActive(id: any) {
    const result = await this.userRepo.findOne({
      where: {
        id: id,
        isActive: 1,
      }
    });
    // console.log('result',result);
    
    if (!result) {
      return false;
    } else {
      return true;
    } 

  
  }

  async getUserForSign(id: string) {
    return await this.userRepo.findOne(id);
  }
  async update(id: string, updateUserDto: UpdateUserDto) {
    const result = await this.userRepo.findOne(id);
    if (!result) throw new NotFoundException('No User Found');
    const {
      firstName,
      lastName,
      isActive,
      isAdmin,
      profilePic,
      phoneNumber,
      isDeleted,
    } = updateUserDto;
    result.firstName = firstName;
    result.lastName = lastName;
    result.isActive = isActive ? true : false;
    result.deletedAt = isDeleted?.toLowerCase() === 'true' ? new Date() : null;
    result.profilePic = profilePic;
    result.phoneNumber = phoneNumber;
    // if (isAdmin)
      result.role = isAdmin ? UserRole.Admin : UserRole.User;
    try {
      await result.save();
    } catch (e) {
      console.log(e);
    }
    return {
      result,
      message: 'User updated successfully',
    };
  }

  async changeNotifications(isActive: boolean, user: User) {
    user.isNotify = isActive;
    await user.save();
    return {
      message: `User Notifications ${
        isActive ? 'enabled' : 'disabled'
      } successfully`,
    };
  }

  async searchUser(name: string, email: string) {
    return await this.userRepo.find({
      where: {
        firstName: new RegExp(`^${name}`),
        lastName: new RegExp(`^${name}`),
      },
    });
  }

  async uploadUserImage(updateUserDto,user:User) {
    const {
      filename,
      is_logo
    } = updateUserDto;
    if(is_logo) {
    //   const updated_user = await this.userRepo.update(
    //     {id:user.id},{
    //      logo: filename
    //      });
    
    //    return updated_user;
    }else {
      const updated_user = await this.userRepo.update(
        {id:user.id},{
         profilePic: filename
         });
    
       return updated_user;
    }
   
    }

  
 
}
