import * as bcrypt from 'bcrypt';
import { UserRole } from 'src/enums/user.enums';
import { ShareFiles } from 'src/modules/file-management/entities/file-management.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ObjectIdColumn,
  Unique,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToMany,
  JoinColumn
} from 'typeorm';
@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: any;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.User,
  })
  role: UserRole;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  @Unique('eMail', ['email'])
  email: string;

  @Column({
    nullable: true,
  })
  profilePic: string;

  @Column({
    nullable: true,
  })
  resetPasswordToken: string;

  @Column({
    nullable: true,
  })
  phoneNumber: string;

  @Column({ nullable: true })
  salt: string;

  @Column()
  password: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @Column({ default: true })
  isActive: boolean;

  @Column({ default: true })
  isNotify: boolean;

  // @Column({
  //   nullable: true,
  // })
  // logo: string;

  @OneToMany(type => ShareFiles, share_files => share_files.initiator_id,{
    onDelete: "CASCADE"
  })
   @JoinColumn({ name: "id" })
   user_id: ShareFiles[];

  async validatePassword(password: string): Promise<boolean> {
    return await bcrypt.compare(password, this.password);
  }
}
