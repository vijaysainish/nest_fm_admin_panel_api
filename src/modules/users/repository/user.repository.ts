import { CreateUserDto } from './../dto/create-user.dto';
import {
  ConflictException,
  InternalServerErrorException,
  NotAcceptableException,
  NotFoundException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UserRole } from 'src/enums/user.enums';
import { EntityRepository, Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { LoginDto } from 'src/modules/auth/dto/login.dto';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async validateUser(loginDto: LoginDto) {
    const { email, password } = loginDto;
    const auth = await this.findOne({
      email,
    });
    if (auth) {
      return auth.validatePassword(password).then((res) => {
        if (res === true) {
          return auth;
        } else {
          throw new NotAcceptableException('Invalid Password');
        }
      });
    } else {
      throw new NotFoundException(`No User Found`);
    }
  }

  async userRegister(createUserDto: CreateUserDto, isAdminRegister: boolean) {
    const {
      email,
      password,
      firstName,
      lastName,
      isActive,
      isAdmin,
      phoneNumber,
    } = createUserDto;
    const newUser = new User();
    const salt = await bcrypt.genSalt(11);
    const passwordHash = (await this.genHash(salt, password)).toString();
    newUser.email = email;
    newUser.password = passwordHash;
    newUser.lastName = lastName;
    newUser.firstName = firstName;
    newUser.phoneNumber = phoneNumber;
    newUser.salt = salt;
    newUser.isActive = isActive;
    newUser.role = isAdmin ? UserRole.Admin : UserRole.User;;
    // newUser.isActive = isAdminRegister
    //   ? true
    //   : isActive?.toLowerCase() == 'true'
    //   ? true
    //   : false;
    // newUser.role =
    //   isAdmin?.toLowerCase() == 'true' ? UserRole.Admin : UserRole.User;
    try {
      await newUser.save();
      return newUser;
    } catch (error) {
      if (error.code) {
        const err = error.code;
        if (err === 11000) {
          throw new ConflictException('Email Already exists');
        }else if(error.code  == 'ER_DUP_ENTRY') {
          throw new ConflictException('Email Already exists');
        }
         else {
          throw new InternalServerErrorException('Something went Wrong'+ error.code);
        }
      } else {
        throw new InternalServerErrorException('Something went Wrong'+ error.code);
      }
    }
  }

  async genHash(salt, password): Promise<string> {
    const hash = await bcrypt.hash(password, salt);
    return hash;
  }
}
