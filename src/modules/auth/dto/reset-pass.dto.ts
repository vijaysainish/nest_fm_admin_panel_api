import { IsString } from 'class-validator';

export class ResetPassDto {
  @IsString()
  resetToken: any;

  @IsString()
  password: string;
}
