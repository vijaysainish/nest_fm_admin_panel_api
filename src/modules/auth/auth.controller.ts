import { JwtAuthGuard } from './auth.guard';
import { ResetPassDto } from './dto/reset-pass.dto';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { CreateUserDto } from './../users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { ForgotPassDto } from './dto/forgot-pass.dto';
import { LoginDto } from './dto/login.dto';
import { ChangePassDto } from './dto/change-pass.dto';
import { GetUser } from 'src/decorators/getUser.decorator';
import { User } from '../users/entities/user.entity';
import { RoleGuard } from './role.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/admin/register')
  create(@Body() adminSignUpDto: CreateUserDto) {
    return this.authService.create(adminSignUpDto);
  }

  @Post('')

  login(@Body() loginDto: LoginDto) {
    return this.authService.login(loginDto);
  }

  @Post('forgot-password')
  forgotPass(@Body() forgotPassDto: ForgotPassDto) {
    return this.authService.forgotPass(forgotPassDto);
  }

  @Patch('reset-password')
  resetPass(@Body() resetPassDto: ResetPassDto) {
    return this.authService.resetPass(resetPassDto);
  }

  @Post('change-password')
  @UseGuards(JwtAuthGuard)
  changePass(@Body() changePassDto: ChangePassDto, @GetUser() user: User) {
    return this.authService.changePass(changePassDto, user);
  }
}
