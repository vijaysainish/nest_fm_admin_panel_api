import { MailerService } from '@nestjs-modules/mailer';
import {
  HttpException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { UserRepository } from '../users/repository/user.repository';
import { CommonService } from './../../common/common/common.service';
import { JwtPayload } from './../../interfaces/jwt-payload.interface';
import { CreateUserDto } from './../users/dto/create-user.dto';
import { ChangePassDto } from './dto/change-pass.dto';
import { ForgotPassDto } from './dto/forgot-pass.dto';
import { LoginDto } from './dto/login.dto';
import { ResetPassDto } from './dto/reset-pass.dto';

@Injectable()
export class AuthService {
  constructor(
    private cs: CommonService,
    private jwtService: JwtService,
    private readonly mailerService: MailerService,
    @InjectRepository(UserRepository) private userRepos: UserRepository,
  ) {}

  async login(loginDto: LoginDto) {
    const response: any = await this.userRepos.validateUser(loginDto);
    // console.log(response.isActive);
    
    if(!response.isActive) {
      throw new HttpException(
        'Your account is deactivated, contact to admin.',
        HttpStatus.UNAUTHORIZED,
      );
    }
    return this.signToken(response);
  }

  async create(createAuthDto: CreateUserDto) {
    const result: any = await this.userRepos.userRegister(createAuthDto, true);
    await this.mailerService
      .sendMail({
        to: result.email,
        // from: 'support@playgroundraters.com',
        subject: 'File Share Registration',
        template: 'src/templates/registration.pug',
        context: {
          name: result.firstName + ' ' + (result.lastName || ''),
          url: ``,
        },
      })
      .then(() => console.log('success'))
      .catch((e) => console.log(e));
    return this.signToken(result);
  }

  async signToken(response: User) {
    const { email, id, role,isActive } = response;
    delete response.resetPasswordToken;
    delete response.password;
    delete response.salt;
    delete response.deletedAt;

    const payload: JwtPayload = {
      id,
      email,
      role,
      isActive
    };

    const accessToken = await this.jwtService.sign(payload);

    return {
      user: response,
      accessToken,
    };
  }

  async forgotPass(forgotPassDto: ForgotPassDto) {
    const result = await this.userRepos.findOne({
      email: forgotPassDto.email,
      isActive: true,
    });
    if (!result) {
      throw new NotFoundException('No User Found');
    }
    result.resetPasswordToken = this.cs.randomString2(20);
    console.log(result.resetPasswordToken);
    
    await result.save();
    await this.mailerService
      .sendMail({
        to: result.email,
        // from: 'support@playgroundraters.com',
        subject: 'Reset Password',
        template: 'src/templates/forgot-password.pug',
        context: {
          name: result.firstName + ' ' + (result.lastName || ''),
          url: `https://dms.miraclechd.co/auth/reset-password/${result.resetPasswordToken}`,
        },
      })
      .then(() => console.log('success'))
      .catch((e) => console.log(e));
    return {
      message: 'Forgot Password requested Successfully',
    };
  }

  async resetPass(resetPassDto: ResetPassDto) {
    const result = await this.userRepos.findOne({
      resetPasswordToken: resetPassDto.resetToken,
      isActive: true,
    });
    if (!result) {
      throw new NotFoundException('No User Found');
    }
    const passwordHash = (
      await this.userRepos.genHash(result.salt, resetPassDto.password)
    ).toString();
    result.password = passwordHash;
    result.resetPasswordToken = null;
    await result.save();
    return {
      message: 'Password changed successfully',
    };
  }

  async changePass(changePassDto: ChangePassDto, user: User) {
    const isValidUser = await this.userRepos.validateUser({
      email: user.email,
      password: changePassDto.oldPassword,
    });
    if (isValidUser instanceof User) {
      const passwordHash = (
        await this.userRepos.genHash(user.salt, changePassDto.newPassword)
      ).toString();
      user.password = passwordHash;
      await user.save();
      return {
        message: 'Password changed Successfully',
      };
    }
    throw new InternalServerErrorException('Something went wrong');
  }
}
