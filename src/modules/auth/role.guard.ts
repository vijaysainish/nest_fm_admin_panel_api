import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import * as jwt from 'jsonwebtoken';
import { UsersService } from '../users/user.service';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private readonly reflector: Reflector,private readonly usersService: UsersService) {}
 async canActivate(
    context: ExecutionContext,
  ):  Promise<boolean>  {
    const request = context.switchToHttp().getRequest();
    if (request) {
      if (!request.headers.authorization) {
        return false;
      }
      console.log(request.headers.authorization);
      const auth = request.headers.authorization;
      const token = auth.split(' ')[1];
      
      const decoded: any = await jwt.verify(token,  '#E$8>%eAXpEQsPh[5spbXUhCkNs;C$]KSAjfM0Q>gfV<inK(1zjR|16:)4g.M@@1SlI{LSF8RKWXM%/OWn&|9;<,L[GO,PPyyAWdG<J)c6a&2mHZdu62rqq+gQ|DkF_Z/*tTbB!aIH1c{o,$.wniFz');
      // console.log(decoded);
      
      // request.me = await this.validateToken(request.headers.authorization);
      let is_acitve = await this.validateRole(decoded, context);
      if(is_acitve) {
        
        return true;
      }else {
        throw new HttpException(
          'Unauthorized request',
          HttpStatus.UNAUTHORIZED,
        );
      }
     
      // throw new HttpException({
      //   status: HttpStatus.UNAUTHORIZED,
      //   error: 'Unauthorized',
      // }, HttpStatus.FORBIDDEN);
    } else {
    return true;
    }
  }
  async validateRole(user, context) {
  let res = await this.usersService.findActive(user.id);
  console.log('res',res);
  
  return res;
    // return true;
    // const roles = this.reflector
    //   .get<string[]>('roles', context.getHandler())
    //   .map(role => role.toUpperCase());
    // if (!roles) {
    //   return true;
    // }

    // const hasRole = () => !!roles.find(item => item === user.roleName);
    // return user && user.roleName && hasRole();

    // use this if user has multiple roles
    // const hasRole = () => user.roles.some((role) => roles.includes(role));
    // return user && user.roles && hasRole();
  }
}
