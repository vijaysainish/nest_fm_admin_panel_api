import { CommonService } from './../../common/common/common.service';
import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from './auth.controller';
import { UserRepository } from '../users/repository/user.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from 'src/strategy/jwt.strategy';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret:
        '#E$8>%eAXpEQsPh[5spbXUhCkNs;C$]KSAjfM0Q>gfV<inK(1zjR|16:)4g.M@@1SlI{LSF8RKWXM%/OWn&|9;<,L[GO,PPyyAWdG<J)c6a&2mHZdu62rqq+gQ|DkF_Z/*tTbB!aIH1c{o,$.wniFz',
      signOptions: { expiresIn: '10d' },
    }),
    TypeOrmModule.forFeature([UserRepository]),
  ],
  controllers: [AuthController],
  providers: [AuthService, CommonService, JwtStrategy],
  exports: [AuthService, JwtStrategy, PassportModule],
})
export class AuthModule {}
